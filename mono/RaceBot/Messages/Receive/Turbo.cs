﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Send;
using SEdge.Common;

namespace RaceBot.Messages.Receive
{
    public class TurboAvailable
    {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;


        public override string ToString()
        {
            return String.Format("Turbo available: {0}ms, or {1} ticks (x{2})!", this.turboDurationMilliseconds, this.turboDurationTicks, this.turboFactor);
        }
    }
}
