﻿using System;
using System.Collections.Generic;

namespace RaceBot.Messages.Receive
{
    /// <summary>
    /// "The carPositions message describes the position of each car on the track."
    /// "The carPositions message will be sent repeatedly on each Server Tick."
    /// </summary>
    public class CarPositions
    {
        public List<CarPosition> carPositions { get; set; }
    }

    public class CarPosition
    {
		public CarId id { get; set; }

        /// <summary>
        /// "The angle depicts the car's slip angle. Normally this is zero, but when you go to a bend fast enough, 
        /// the car's tail will start to drift. Naturally, there are limits to how much you can drift without 
        /// crashing out of the track."
        /// </summary>
        public double angle { get; set; }

        public PiecePosition piecePosition { get; set; }
    }

    public class PiecePosition
    {
        /// <summary>
        /// "Zero based index of the piece the car is on."
        /// </summary>
        public int pieceIndex { get; set; }

        /// <summary>
        /// "The distance the car's Guide Flag has traveled from the start of the piece along the current lane."
        /// </summary>
        public double inPieceDistance { get; set; }

        /// <summary>
        /// "A pair of lane indices. Usually startLaneIndex and endLaneIndex are equal,
        /// but they do differ when the car is currently switching lane."
        /// </summary>
        public Lane lane { get; set; }

        /// <summary>
        /// "the number of laps the car has completed. The number 0 indicates that the car is on its first lap.
        /// The number -1 indicates that it has not yet crossed the start line to begin it's first lap."
        /// </summary>
        public int lap { get; set; }
    }

    public class Lane
    {
        public static Lane CreateLane(int oneLane)
        {
            return new Lane() {startLaneIndex = oneLane, endLaneIndex = oneLane};
        }

        public int startLaneIndex { get; set; }
        public int endLaneIndex { get; set; }
    }
}

