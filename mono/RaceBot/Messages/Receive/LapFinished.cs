﻿using System;

namespace RaceBot.Messages.Receive
{
    /// <summary>
    /// "Server sends lapFinished message when a car completes a lap."
    /// </summary>
    public class LapFinished
    {
        public CarId car { get; set; }
        public LapTime lapTime { get; set; }
        public RaceTime raceTime { get; set; }
        public Ranking ranking { get; set; }

        public override string ToString ()
        {
            return String.Format ("{0} finished lap {1} in {2} seconds, ranking {3}.",
                this.car.name,
                this.lapTime.lap + 1,
                this.lapTime.millis / 1000f,
                this.ranking.overall);
        }
    }

    public class LapTime
    {
        public int lap { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }

    public class RaceTime
    {
        public int laps { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }

    public class Ranking
    {
        public int overall { get; set; }
        public int fastestLap { get; set; }
    }
}

