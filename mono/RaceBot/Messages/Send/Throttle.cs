using System;

namespace RaceBot.Messages.Send
{
    public class Throttle : SendMsg
    {
		public const float minThrottle = 0.0f;
		public const float maxThrottle = 1.0f;

        public double value;

		/// <summary>
		/// "Accepted values are between 0.0 and 1.0. The value 1.0 indicates full throttle."
		/// </summary>
		/// <param name="value">Value.</param>
        public Throttle(double value)
        {
			if (value < minThrottle) {
				this.value = minThrottle;
			} else if (value > maxThrottle)
				this.value = maxThrottle;
			else {
				this.value = value;
			}
		    if (double.IsNaN(this.value))
		        this.value = 1;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}