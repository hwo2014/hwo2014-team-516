namespace RaceBot.Messages.Send
{
    /// <summary>
    /// Send this "join" message to automatically start a new quick race.
    /// </summary>
    public class Join : SendMsg
    {
        /// <summary>
        /// Name of the bot. Maximum length for bot name is 16 characters. 
        /// </summary>
        public string name;
        public string key;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}