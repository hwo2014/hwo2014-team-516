﻿using System;
using RaceBot.Messages.Send;

namespace RaceBot
{
    /// <summary>
    /// Use this message to join and/or create a race with specific parameters.
    /// </summary>
    public class JoinRace : Join
    {
        public BotId botId;

        public string trackName;
        public string password;
        public int carCount;

        public JoinRace (string name, string key, string trackName, int carCount, string password) : base(name, key)
        {
            this.botId = new BotId (name, key);
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }

        protected override string MsgType ()
        {
            return "joinRace";
        }
    }

    public class BotId {
        public string name;
        public string key;

        public BotId(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }
}

