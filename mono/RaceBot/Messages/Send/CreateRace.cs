﻿using System;
using RaceBot.Messages.Send;

namespace RaceBot
{
    /// <summary>
    /// Use this message to join and/or create a race with specific parameters.
    /// </summary>
    public class CreateRace : Join
    {
        public BotId botId;

        public string trackName;
        public string password;
        public int carCount;

        public CreateRace(string name, string key, string trackName, int carCount, string password)
            : base(name, key)
        {
            this.botId = new BotId(name, key);
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }
}

