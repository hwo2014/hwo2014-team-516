﻿using System;

namespace RaceBot.Messages.Send
{
	public enum SwitchDirection
	{
        None,
		Left,
        Right
	}

	/// <summary>
	/// "Bot sends switchLane to switch between the lanes on the track.
	/// You may switch either to Left or Right."
	/// 
	/// "The lane switch will occur at the first possible switch after 
	/// message has been received. If multiple commands are sent, only 
	/// the latest will apply. Only a single switch can per car can
	/// occur on the same track piece at a time, regardless of the number 
	/// of lanes on the track."
	/// </summary>
	public class SwitchLane : SendMsg
	{
        private string switchTo;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="switchTo">Direction where you want to change track to.</param>
		public SwitchLane(SwitchDirection switchTo)
		{
            if (switchTo == SwitchDirection.Left || switchTo == SwitchDirection.Right) {
                this.switchTo = switchTo.ToString ();
            } else {
                throw new Exception ("Can not switch direction to " + switchTo.ToString ());
            }
		}

		protected override string MsgType ()
		{
			return "switchLane";
		}

		protected override object MsgData ()
		{
			return this.switchTo;
		}
	}
}

