using System;

namespace RaceBot.Messages.Send
{
    public class MsgWrapper
	{
	    public string msgType;
	    public Object data;
        public string gameId;
        public long? gameTick;

        public MsgWrapper(string msgType, Object data, long gameTick)
	    {
	        this.msgType = msgType;
	        this.data = data;
            this.gameTick = gameTick;
	    }
	}
}