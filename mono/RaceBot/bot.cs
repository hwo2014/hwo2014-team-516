using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RaceBot.AI;
using RaceBot.Messages.Receive;
using RaceBot.Messages.Send;
using RaceBot.Race;
using RaceBot.Utilities;
using SEdge.Common;
using RaceTime = RaceBot.Race.RaceTime;
using System.Diagnostics;

namespace RaceBot
{
    public class Bot
    {
        private StreamWriter writer;

        private RacingAI ai;
        private RaceLogger logger;
        private RaceKnowledge knowledge;
        private AiFactory.AiType type = AiFactory.AiType.SpinRacer;
        private string aiString;

        private Stopwatch watch;

        /// <summary>
        /// The last received message from server.
        /// </summary>
        private MsgWrapper lastMsg;

        public Bot(StreamWriter writer, Join join, string ai)
        {
            this.writer = writer;
            this.aiString = ai;

            if (aiString.Contains("Const"))
                this.type = AiFactory.AiType.Const;
            else if(aiString.Contains("TheOne"))
                this.type = AiFactory.AiType.TheOne;
            else if(aiString.Contains(AiFactory.AiType.CornerTest.ToString()))
                this.type = AiFactory.AiType.CornerTest;
            else
                this.type = (AiFactory.AiType)Enum.Parse(typeof(AiFactory.AiType), aiString);

            knowledge = new RaceKnowledge(type == AiFactory.AiType.SpinRacer);
#if DEBUG
            logger = new RaceLogger(knowledge, false, true, join.name, writeAllToFile: true);
#else
            logger = new RaceLogger(knowledge, false, false, join.name, writeAllToFile: true);
#endif
            knowledge.Logger = logger;
            
            send(join);
        }

        public void Play(StreamReader reader)
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                watch = Stopwatch.StartNew();

                lastMsg = JsonConvert.DeserializeObject<MsgWrapper>(line);

                if (lastMsg.gameTick.HasValue)
                {
                    knowledge.RaceTime.Update (lastMsg.gameTick.Value);
                }

                switch (lastMsg.msgType)
                {
                    case "join":
                        JoinHandler (lastMsg);
                        break;
                    case "yourCar":
                        YourCarHandler (lastMsg);
                        break;
                    case "gameInit":
                        GameInitHandler (lastMsg);
                        break;
                    case "gameStart":
                        GameStartHandler (lastMsg);
                        break;
                    case "carPositions":
                        // Switch cases are in the order in which they appear in the game,
                        // however the very first carPositions message is sent right before gameStart.
                        CarPositionsHandler (lastMsg);
                        break;
                    case "turboAvailable":
                        TurboAvailableHandler(lastMsg);
                        break;
                    case "turboStart":
                        TurboStartHandler(lastMsg);
                        break;
                    case "turboEnd":
                        TurboEndHandler(lastMsg);
                        break;
                    case "crash":
                        CrashHandler (lastMsg);
                        break;
                    case "spawn":
                        SpawnHandler (lastMsg);
                        break;
                    case "lapFinished":
                        LapFinishedHandler (lastMsg);
                        break;
                    case "finish":
                        FinishHandler (lastMsg);
                        break;
                    case "gameEnd":
                        GameEndHandler (lastMsg);
                        break;
                    case "tournamentEnd":
                        TournamentEndHandler (lastMsg);
                        break;
                    case "dnf":
                        DnfHandler (lastMsg);
                        break;
                    default:
                        DefaultHandler (line);
                        break;
                }
            }
            logger.CloseLog();
        }

        /// <summary>
        /// Sends a message to the server.
        /// </summary>
        /// <param name="msg">Message to send.</param>
        private void send(SendMsg msg)
        {
            msg.gameTick = knowledge.RaceTime.Ticks;
            writer.WriteLine(msg.ToJson());

            if (watch != null)
            {
                watch.Stop();
                logger.DecisionTimeTaken = watch.ElapsedMilliseconds;

                if (watch.ElapsedMilliseconds > 25)
                {
                    knowledge.Logger.WriteLine("!!! DECISION TIME " + watch.ElapsedMilliseconds + " MILLISECONDS !!!");
                }
            }

            // Uncomment below line if you want to print all sent messages to console.
            //this.PrintMessage (msg.ToJson(), true);
        }

        /// <summary>
        /// Prints a JSON message to console.
        /// </summary>
        /// <param name="jsonMessage">Json to print.</param>
        /// <param name="send">If a flag that can be used that this message was sent to the server.
        /// Default assumption is that we are printing received messages.</param>
        private void PrintMessage(string jsonMessage, bool send = false)
        {
            knowledge.Logger.WriteLine ();
            knowledge.Logger.WriteLine ("!!!" + (send ? "SENT" : "RECEIVED") + " MESSAGE!!!");

            try
            {
                JObject json = JObject.Parse(jsonMessage);
                knowledge.Logger.WriteLine (json);
            }
            catch(JsonReaderException ex)
            {
                knowledge.Logger.WriteLine (jsonMessage);
                knowledge.Logger.WriteLine ();
                knowledge.Logger.WriteLine (ex);
            }

            knowledge.Logger.WriteLine ("!!!" + (send ? "SENT" : "RECEIVED") + " MESSAGE!!!");
            knowledge.Logger.WriteLine ();
        }

        private void JoinHandler(MsgWrapper msg)
        {
            knowledge.Logger.WriteLine("Joined");
        }

        private void YourCarHandler(MsgWrapper msg)
        {
            JObject yourCar = JObject.Parse (msg.data.ToString());
            string name = yourCar.GetValue ("name").ToString();
            string color = yourCar.GetValue ("color").ToString();

            knowledge.UpdateMyCar(name);

            knowledge.Logger.WriteLine(String.Format("My own car is {0} ({1})", name, color));
        }

        private void GameInitHandler(MsgWrapper msg)
        {
            var gameInitData = JsonConvert.DeserializeObject<GameInit> (msg.data.ToString ());

            knowledge.Initialize(gameInitData);
            logger.StartLog();

            if (ai == null)
            {
#if DEBUG
            if(type == AiFactory.AiType.Const)
            {
                var constString = "";
                for (var i = 5; i < aiString.Length; i++)
                    constString += aiString[i];
                ai = AiFactory.GenerateAi(knowledge, type, ZLib.ParseFloat(constString));
            }
            else if (type == AiFactory.AiType.TheOne)
            {
                var laneString = "";
                for (var i = 6; i < aiString.Length; i++)
                    laneString += aiString[i];
                ai = AiFactory.GenerateAi(knowledge, type, ZLib.ParseFloat(laneString));
            }
            else if(type == AiFactory.AiType.CornerTest)
            {
                var targetFc = "";
                for(var i = 10; i < aiString.Length; i++)
                    targetFc += aiString[i];
                ai = AiFactory.GenerateAi(knowledge, type, ZLib.ParseFloat(targetFc));
            }
            else
                ai = AiFactory.GenerateAi(knowledge, type);
#else
                ai = AiFactory.GenerateSpinRacerAi(knowledge);
#endif

            }

            knowledge.Logger.WriteLine("Crash angle: " + RaceMath.CrashAngle);

            knowledge.Logger.WriteLine(gameInitData.ToString ());
        }

        private void GameStartHandler(MsgWrapper msg)
        {
            if (TurboDecision.ForcedThrottle)
            {
                TurboDecision.ResetForcedThrottle();
                knowledge.Logger.WriteLine("Forced throttle reset.");
            }

            // There is no data with gameStart message, but game tick is 0 (zero) at this point.
            knowledge.Logger.WriteLine("Race starts, received game tick is " + msg.gameTick + ", race time tick is " + knowledge.RaceTime.Ticks);
            send(new Throttle(Throttle.maxThrottle));
        }

        /// <summary>
        /// "carPositions" is the single most important message to be handled.
        /// Adding the comment so this method would stand out more.
        /// </summary>
        private void CarPositionsHandler(MsgWrapper msg)
        {
            CarPosition[] carPositions = JsonConvert.DeserializeObject<CarPosition[]>(msg.data.ToString());

            try 
            {
                ai.Execute (knowledge.RaceTime, carPositions);
            } catch(Exception ex)
            {
                this.PrintMessage(ex.ToString());
            }
            

            if (lastMsg != null
                && lastMsg.msgType == "carPositions"
                && !(lastMsg.gameTick > 1))
            {
                send(new Throttle(Throttle.maxThrottle));
                knowledge.Logger.WriteLine("FORCE THROTTLE 1.0!!!");
            }
            else
            {
                
                if (ai.Knowledge.ShouldSwitchLane())
                {
                    if (ai.Knowledge.ShouldUseTurbo())
                    {
                        ai.Knowledge.CouldNotSendTurbo();
                    }
                    //if (knowledge.LastSentThrottle == 1 && knowledge.Throttle == 0)
                    //{
                    //    logger.WriteLine("Could not change lanes, might crash!");
                    //    send(new Throttle(ai.Knowledge.Throttle));
                    //    ai.Knowledge.SwitchLaneSent(); 
                    //}
                    //else
                    //{
                        send(new SwitchLane(ai.Knowledge.SwitchDirection));
                        //knowledge.Logger.WriteLine("switch: " + ai.Knowledge.SwitchDirection);
                        ai.Knowledge.SwitchLaneSent();
                    //}
                }
                else
                {
                    if (ai.Knowledge.ShouldUseTurbo())
                    {
                        send(new Turbo("WRUUUUUUUUM!"));
                        knowledge.TurboSent();
                    }
                    else
                    {
                        knowledge.LastSentThrottle = ai.Knowledge.Throttle;
                        send(new Throttle(ai.Knowledge.Throttle));
                    }
                        
                }    
            }

            logger.Update(knowledge.RaceTime);
        }


        private void TurboAvailableHandler(MsgWrapper msg)
        {
            var turboData = JsonConvert.DeserializeObject<TurboAvailable>(msg.data.ToString());
            knowledge.Logger.WriteLine(turboData.ToString());
            knowledge.UpdateTurboAvailable(turboData);
        }

        void TurboStartHandler(MsgWrapper msg)
        {
            JObject json = JObject.Parse(msg.data.ToString());
            string name = json.GetValue("name").ToString();
            this.knowledge.UpdateTurboStart(name);
        }

        void TurboEndHandler(MsgWrapper msg)
        {
            JObject json = JObject.Parse(msg.data.ToString());
            string name = json.GetValue("name").ToString();
            this.knowledge.UpdateTurboEnd(name);
        }

        private void CrashHandler(MsgWrapper msg)
        {
            var crashData = JsonConvert.DeserializeObject<Crash> (msg.data.ToString ());

            knowledge.Logger.WriteLine (crashData.ToString ());
            

            knowledge.UpdateCrash(crashData);
            logger.ReportCrash();
        }

        private void SpawnHandler(MsgWrapper msg)
        {
            var spawnData = JsonConvert.DeserializeObject<Spawn> (msg.data.ToString ());

            knowledge.Logger.WriteLine (spawnData.ToString ());
            knowledge.UpdateSpawn(spawnData);
        }

        private void LapFinishedHandler(MsgWrapper msg)
        {
            var lapFinishedData = JsonConvert.DeserializeObject<LapFinished> (msg.data.ToString ());

            knowledge.Logger.WriteLine(lapFinishedData.ToString ());
            logger.LapFinished();

            knowledge.UpdateLap (lapFinishedData);

            if (knowledge.IsQualifyingPeriod)
            {
                logger.WriteLine("Qualification time left: " + Math.Round(knowledge.QualifyingTimeLeft / 1000f, 3)
                    + " my best lap time so far: " + Math.Round(knowledge.MyCar.BestLapTime / 1000f, 3)
                    + " out of " + knowledge.MyCar.CompletedLaps + " laps");
            }
        }

        private void FinishHandler(MsgWrapper msg)
        {
            JObject finish = JObject.Parse (msg.data.ToString());
            string name = finish.GetValue ("name").ToString();
            knowledge.UpdateCarFinished(name);
        }

        private void GameEndHandler(MsgWrapper msg)
        {
            if (TurboDecision.ForcedThrottle)
            {
                TurboDecision.ResetForcedThrottle();
                knowledge.Logger.WriteLine("Forced throttle reset.");
            }

            var gameEndData = JsonConvert.DeserializeObject<GameEnd> (msg.data.ToString ());

            knowledge.Logger.WriteLine (gameEndData.ToString ());

            knowledge.Logger.WriteLine("TURBO STATISTICS");
            foreach (RaceCar car in knowledge.Cars)
            {
                string message = String.Format("- {0} received {1}, and used {2} turbos", car.carId, car.TurbosReceived, car.TurbosUsed);
                knowledge.Logger.WriteLine(message);
            }
            knowledge.Logger.WriteLine("Total turbos " + knowledge.TotalTurbos);
            knowledge.Logger.WriteLine();

            knowledge.Logger.WriteLine("CRASH STATISTICS");
            foreach (RaceCar car in knowledge.Cars)
            {
                string message = String.Format("- {0} crashed {1} times", car.carId, car.CrashCount);
                knowledge.Logger.WriteLine(message);
            }
            knowledge.Logger.WriteLine();

            knowledge.Logger.WriteLine(String.Format("Track: {0} AI: {1}", knowledge.Track.Name, this.aiString));
            knowledge.RaceFinished();
            logger.WriteSectionData(knowledge.RaceTime);
        }

        private void TournamentEndHandler(MsgWrapper msg)
        {
            // No data with this message
            knowledge.Logger.WriteLine ("Tournament has ended.");
        }

        private void DnfHandler(MsgWrapper msg)
        {
            var dnfData = JsonConvert.DeserializeObject<Dnf> (msg.data.ToString ());
            knowledge.Logger.WriteLine(dnfData.ToString());
            knowledge.UpdateDnf(dnfData);
        }

        private void DefaultHandler(string line)
        {
            this.PrintMessage (line);
        }
    }
}
