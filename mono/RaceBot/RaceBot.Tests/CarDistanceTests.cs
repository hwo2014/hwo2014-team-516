﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using RaceBot.AI;
using RaceBot.Messages.Receive;
using RaceBot.Race;

namespace RaceBot.Tests
{
    [TestFixture]
    public class CarDistanceTests
    {
        [Test]
        public void UsaCarIsAway()
        {
            Track track = TestUtils.ReadTestTrackFromFile(TestUtils.PathToUsaTrack);
            var positions = new List<PiecePosition>();
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 50,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 1
            });
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 80,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 1
            });
            var knowledge = TestUtils.CreateRaceKnowledge(track, positions);

            var d = knowledge.Track.DistanceToNextCar(knowledge.Cars[0], knowledge.Cars[1]);
            Assert.AreEqual(30, d);
        }

        [Test]
        public void UsaCarIsBehind()
        {
            Track track = TestUtils.ReadTestTrackFromFile(TestUtils.PathToUsaTrack);
            var positions = new List<PiecePosition>();
            positions.Add(new PiecePosition()
                         {
                             inPieceDistance = 50,
                             lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                             lap = 1,
                             pieceIndex = 1
                         });
            positions.Add(new PiecePosition()
                         {
                             inPieceDistance = 0,
                             lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                             lap = 1,
                             pieceIndex = 1
                         });
            var knowledge = TestUtils.CreateRaceKnowledge(track, positions);

            var d = knowledge.Track.DistanceToNextCar(knowledge.Cars[0], knowledge.Cars[1]);
            Assert.AreEqual(-50, d);
        }

        [Test]
        public void UsaCarIsfurtherBehind()
        {
            Track track = TestUtils.ReadTestTrackFromFile(TestUtils.PathToUsaTrack);
            var positions = new List<PiecePosition>();
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 50,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 2,
                pieceIndex = 0
            });
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 0,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 31
            });
            var knowledge = TestUtils.CreateRaceKnowledge(track, positions);

            var d = knowledge.Track.DistanceToNextCar(knowledge.Cars[0], knowledge.Cars[1]);
            Assert.AreEqual(-150, d);
        }

        [Test]
        public void UsaCarIsFarAway()
        {
            Track track = TestUtils.ReadTestTrackFromFile(TestUtils.PathToUsaTrack);
            var positions = new List<PiecePosition>();
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 50,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 1
            });
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 80,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 13
            });
            var knowledge = TestUtils.CreateRaceKnowledge(track, positions);

            var d = knowledge.Track.DistanceToNextCar(knowledge.Cars[0], knowledge.Cars[1]);
            Assert.AreEqual(1000, d);
        }

        [Test]
        public void UsaCarIsFarBehind()
        {
            Track track = TestUtils.ReadTestTrackFromFile(TestUtils.PathToUsaTrack);
            var positions = new List<PiecePosition>();
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 50,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 1
            });
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 0,
                lane = new Lane() { endLaneIndex = 1, startLaneIndex = 1 },
                lap = 1,
                pieceIndex = 27
            });
            var knowledge = TestUtils.CreateRaceKnowledge(track, positions);

            var d = knowledge.Track.DistanceToNextCar(knowledge.Cars[0], knowledge.Cars[1]);
            Assert.Less(d, -300);
        }

        [Test]
        public void UsaCarIsOtherLaneAlmostEven()
        {
            Track track = TestUtils.ReadTestTrackFromFile(TestUtils.PathToUsaTrack);
            var positions = new List<PiecePosition>();
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 16,
                lane = new Lane() { endLaneIndex = 2, startLaneIndex = 2 },
                lap = 1,
                pieceIndex = 5
            });
            positions.Add(new PiecePosition()
            {
                inPieceDistance = 15,
                lane = new Lane() { endLaneIndex = 0, startLaneIndex = 0 },
                lap = 1,
                pieceIndex = 5
            });
            var knowledge = TestUtils.CreateRaceKnowledge(track, positions);

            var d = knowledge.Track.DistanceToNextCar(knowledge.Cars[0], knowledge.Cars[1]);
            Assert.Greater(d, 0);
        }
    }
}
