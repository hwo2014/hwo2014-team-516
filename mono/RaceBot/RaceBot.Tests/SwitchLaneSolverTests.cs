﻿using System;
using RaceBot.Messages.Receive;
using NUnit.Framework;
using RaceBot.Race;
using System.Linq;
using System.Collections.Generic;
using RaceBot.Utilities;

namespace RaceBot.Tests
{
    [TestFixture]
    public partial class SwitchLaneSolverTests
    {
        // NOTE THAT THESE VALUES WILL PROBABLY ONLY BE TRUE FOR TWO LANE TRACKS!!!
        private const int LeftLaneIndex = 0;
        private const int RightLaneIndex = 1;

        private SwitchLaneSolver CreateSolver(string pathToFile)
        {
            Track track = TestUtils.ReadTestTrackFromFile (pathToFile);
            RaceTrack raceTrack = new RaceTrack (track);
            //PrintSwitchPieceIndexes(raceTrack);
            return raceTrack.LaneSolver;
        }

        private static void PrintSwitchPieceIndexes (RaceTrack raceTrack)
        {
            int i = 1;
            foreach (var switchBlock in raceTrack.Pieces) {
                if (switchBlock.HasSwitch) {
                    Console.WriteLine (i + ". switch piece index in " + raceTrack.Name +  ": " + switchBlock.Index);
                    i++;
                }
            }
        }

        /// <summary>
        /// Utility method for creating a PiecePosition object for the tests.
        /// </summary>
        /// <param name="carOnPiece">Car on piece.</param>
        private PiecePosition CreatePiecePosition(int carOnPiece, int lane = 0)
        {
            return new PiecePosition {
                pieceIndex = carOnPiece,
                lane = new Lane {
                    startLaneIndex = lane,
                    endLaneIndex = lane
                }
            };
        }

        #region Keimola tests

        [Test]
        public void OptimumLaneKeimolaSwitch1()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(2));

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch2L0()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(7, 0));

            // Assert
            Assert.AreEqual (LeftLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch2L1()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver(TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(7, 0));

            // Assert
            Assert.AreEqual(LeftLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch3()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(12)); 

            // Assert
            Assert.AreEqual (LeftLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch4()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(17)); 

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch5()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(24)); 

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch6()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(28));

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch7()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToKeimolaTrack);
            var pos = CreatePiecePosition(34);
            pos.lane.startLaneIndex = 0;
            pos.lane.endLaneIndex = 0;

            // Act
            int optimumLaneIndex = solver.GetOptimumLane(pos); 

            // Assert
            Assert.AreEqual(pos.lane.endLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneKeimolaSwitch7V2()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver(TestUtils.PathToKeimolaTrack);
            var pos = CreatePiecePosition(34);
            pos.lane.startLaneIndex = 1;
            pos.lane.endLaneIndex = 1;

            // Act
            int optimumLaneIndex = solver.GetOptimumLane(pos);

            // Assert
            Assert.AreEqual(pos.lane.endLaneIndex, optimumLaneIndex);
        }

        #endregion

        #region Germany tests

        [Test]
        public void OptimumLaneGermanySwitch1On0()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(0,0)); 

            // Assert
            Assert.AreEqual (LeftLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch1On1()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver(TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(0,1));

            // Assert
            Assert.AreEqual(LeftLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch2()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(13)); 

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch3()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(20)); 

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch4()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(26)); 

            // Assert
            Assert.AreEqual (LeftLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch5()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(35)); 

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex); // This is questionable, since the right, inner lane is VERY STEEP
        }

        [Test]
        public void OptimumLaneGermanySwitch6()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);
            
            // Act
            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(35,0));

            // Assert
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
            
        }

        [Test]
        public void OptimumLaneGermanySwitch6L1()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver(TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(35, 1));

            // Assert
            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch7()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(52)); 

            // Assert
            // Expecting right lane = 1
            Assert.AreEqual (RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneGermanySwitch7L1()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateSolver(TestUtils.PathToGermanyTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(52, 1));

            // Assert
            // Expecting right lane = 1
            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }
        #endregion

        #region Usa tests



        [Test]
        public void OptimumLaneUsaSwitch1()
        {
            // Arrange 
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToUsaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(0));

            // Assert
            Assert.AreEqual (2, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneUsaSwitch2()
        {
            // Arrange 
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToUsaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(6));

            // Assert
            Assert.AreEqual (2, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneUsaSwitch3()
        {
            // Arrange 
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToUsaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(16));

            // Assert
            Assert.AreEqual (2, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneUsaSwitch4()
        {
            // Arrange 
            SwitchLaneSolver solver = this.CreateSolver (TestUtils.PathToUsaTrack);

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(22));

            // Assert
            Assert.AreEqual (2, optimumLaneIndex);
        }

        #endregion

        #region France tests

        [Test]
        public void OptimumLaneFranceSwitch1()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(0));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneFranceSwitch2()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(3));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneFranceSwitch2On1()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(3,1));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneFranceSwitch3()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(15));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneFranceSwitch4()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(19));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneFranceSwitch5()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(25));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneFranceSwitch6()
        {
            var solver = CreateSolver(TestUtils.PathToFranceTrack);

            int optimumLaneIndex = solver.GetOptimumLane(CreatePiecePosition(32));

            Assert.AreEqual(RightLaneIndex, optimumLaneIndex);
        }

        #endregion
    }
}

