﻿using System;
using NUnit.Framework;
using RaceBot.Utilities;

namespace RaceBot.Tests
{
    [TestFixture]
    public class RaceMathTests
    {
        [Test]
        public void PositiveSlopeOfLinearRegression()
        {
            // Arrange
            double[] x = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            double[] y = { 3, 2, 4, 3, 6, 5, 6, 5, 8, 8 };

            // Act
            double slope = RaceMath.SlopeOfLinearRegression(x, y);

            // Assert
            Assert.AreEqual(0.606060d, slope, 0.00001d);
        }

        [Test]
        public void NegativeSlopeOfLinearRegression()
        {
            // Arrange
            double[] x = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            double[] y = { 3, -1, 2, 1, 3, 1, -2, -1, 1, -2, -1, -2 };

            // Act
            double slope = RaceMath.SlopeOfLinearRegression(x, y);

            // Assert
            Assert.AreEqual(-0.342657, slope, 0.00001d);
        }

        [Test]
        public void CentrifugalForce1()
        {
            // Arrange
            double velocity = 312.5d;
            double radius = 90;

            // Act
            double centrifugalForce = RaceMath.CentrifugalForce(velocity, radius);

            // Assert
            // Expecting positive 1085.069444
            Assert.AreEqual(1085.069444d, centrifugalForce, 0.000001d);
        }

        [Test]
        public void CentrifugalForce2()
        {
            // Arrange
            double velocity = 381.35d;
            double radius = 150;

            // Act
            double centrifugalForce = RaceMath.CentrifugalForce(velocity, radius);

            // Assert
            // Expecting positive 969.5188167
            Assert.AreEqual(969.5188167, centrifugalForce, 0.000001d);
        }
    }
}

