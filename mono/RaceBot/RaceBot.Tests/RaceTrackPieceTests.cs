﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RaceBot.Messages.Receive;
using RaceBot.Race;

namespace RaceBot.Tests
{
	[TestFixture]
	public class RaceTrackPieceTests
	{
		[Test]
		public void StraightPieceIsDetected()
		{
			// Arrange
			Piece piece = new Piece ();

			piece.length = 100;
			piece.@switch = null;
			piece.angle = null;
			piece.radius = null;

			// Act
			RaceTrackPiece trackPiece = new RaceTrackPiece (piece);

			// Assert
			Assert.IsTrue (trackPiece.IsStraight ());
			Assert.IsFalse (trackPiece.IsBend ());
		}

		[Test]
		public void BendPieceIsDetected()
		{
			// Arrange
			Piece piece = new Piece ();

			piece.length = 0.0d;
			piece.@switch = null;
			piece.angle = 45d;
			piece.radius = 100;

			// Act
			RaceTrackPiece trackPiece = new RaceTrackPiece (piece);

			// Assert
			Assert.IsTrue (trackPiece.IsBend ());
			Assert.IsFalse (trackPiece.IsStraight ());
		}

        [Test]
        public void SwitchPieceIsDetected()
        {
            // Arrange
            Piece straightPiece = new Piece();
            straightPiece.length = 100;
            straightPiece.@switch = true;
            straightPiece.angle = null;
            straightPiece.radius = null;

            Piece bendPiece = new Piece ();
            bendPiece.length = 0.0d;
            bendPiece.@switch = true;
            bendPiece.angle = 45d;
            bendPiece.radius = 100;

            // Act
            RaceTrackPiece straightSwitch = new RaceTrackPiece(straightPiece);
            RaceTrackPiece bendSwitch = new RaceTrackPiece(bendPiece);

            // Assert
            Assert.IsTrue(straightSwitch.HasSwitch);
            Assert.IsTrue(bendSwitch.HasSwitch);
        }

		[Test]
		public void RightTurnIsDetected()
		{
			// Arrange
			Piece piece = new Piece ();
			piece.angle = 45.0f;
			piece.radius = 100;

			// Act
			RaceTrackPiece trackPiece = new RaceTrackPiece (piece);

			// Assert
			Assert.IsTrue (trackPiece.IsRightTurn ());
			Assert.IsFalse (trackPiece.IsLeftTurn ());
		}

		[Test]
		public void LeftTurnIsDetected()
		{
			// Arrange
			Piece piece = new Piece ();
			piece.angle = -45.0f;
			piece.radius = 100;

			// Act
			RaceTrackPiece trackPiece = new RaceTrackPiece (piece);

			// Assert
			Assert.IsTrue (trackPiece.IsLeftTurn ());
			Assert.IsFalse (trackPiece.IsRightTurn ());
		}

		[Test]
		public void BendPieceLengthWithPositiveAngle()
		{
			// Arrange
			Piece piece = new Piece ();
			piece.angle = 30.0f;
			piece.radius = 100;
            RaceTrack usa = TestUtils.LoadUsa();

			// Act
            RaceTrackPiece trackPiece = new RaceTrackPiece (piece, usa);

			// Assert
            Assert.AreEqual (52.3599f, trackPiece.GetLength (Lane.CreateLane(1)), 0.0001f);
		}

		[Test]
		public void BendPieceLengthWithNegativeAngle()
		{
			// Arrange
			Piece piece = new Piece ();
			piece.angle = -30.0f;
			piece.radius = 100;
            RaceTrack usa = TestUtils.LoadUsa();

			// Act
            RaceTrackPiece trackPiece = new RaceTrackPiece (piece, usa);

			// Assert
            Assert.AreEqual (52.3599f, trackPiece.GetLength (Lane.CreateLane(1)), 0.0001f);
		}

        [Test]
        public void BendPieceLengthWithLaneOffsetAndLeftTurn()
        {
            // Arrange
            Piece piece = new Piece();
            piece.angle = -30.0f;
            piece.radius = 100;
            RaceTrack usa = TestUtils.LoadUsa();

            // Act
            RaceTrackPiece trackPiece = new RaceTrackPiece (piece, usa);
            var length0 = trackPiece.GetLength (Lane.CreateLane(0));
            var length1 = trackPiece.GetLength (Lane.CreateLane(1));
            var length2 = trackPiece.GetLength (Lane.CreateLane(2));

            Assert.AreEqual(41.8879f, length0, 0.0001f);
            Assert.AreEqual(52.3599f, length1, 0.0001f);
            Assert.AreEqual(62.8319f, length2, 0.0001f);
        }

        [Test]
        public void BendRadiusLeftTurn()
        {
            // Arrange
            Piece piece = new Piece();
            piece.angle = -45.0f;
            piece.radius = 100;
            RaceTrack usa = TestUtils.LoadUsa();
            RaceTrackPiece trackPiece = new RaceTrackPiece(piece, usa);

            // Act
            var radius = trackPiece.GetRadius(Lane.CreateLane(0));

            // Assert
            Assert.AreNotEqual(100, radius);
            Assert.AreEqual(80, radius);
        }

        [Test]
        public void BendRadiusRightTurn()
        {
            // Arrange
            Piece piece = new Piece();
            piece.angle = 45.0f;
            piece.radius = 100;
            RaceTrack usa = TestUtils.LoadUsa();
            RaceTrackPiece trackPiece = new RaceTrackPiece(piece, usa);

            // Act
            var radius = trackPiece.GetRadius(Lane.CreateLane(0));

            // Assert
            Assert.AreNotEqual(100, radius);
            Assert.AreEqual(120, radius);
        }

		[Test]
        public void BendPieceLengthWithLaneOffsetAndRightTurn()
		{
            // distanceFromCenter: "A positive value tells that the lanes is to the right
            // from the center line while a negative value indicates a lane to the left from the center."

            // Therefore when turning to the left, the sign of the distanceFromCenter should be reversed.

			// Arrange
			Piece piece = new Piece ();
			piece.angle = 30.0f;
			piece.radius = 100;
            RaceTrack usa = TestUtils.LoadUsa();
            RaceTrackPiece trackPiece = new RaceTrackPiece (piece, usa);

			// Act
            var length0 = trackPiece.GetLength (Lane.CreateLane(0)); // Should increase radius by 20
            var length1 = trackPiece.GetLength(Lane.CreateLane(1));
            var length2 = trackPiece.GetLength(Lane.CreateLane(2)); // Should decrease radius by 20

            // Assert
            Assert.AreEqual(62.8319f, length0, 0.0001f);
			Assert.AreEqual(52.3599f, length1, 0.0001f);
            Assert.AreEqual(41.8879f, length2, 0.0001f);
		}

        [Test]
        public void BendSteepness()
        {
            // Arrange
            Piece piece = new Piece ();
            piece.angle = 90.0f;
            piece.radius = 100;

            RaceTrack usa = TestUtils.LoadUsa();
            RaceTrackPiece trackPiece = new RaceTrackPiece (piece, usa);

            // Act
            double steepness = trackPiece.GetBendSteepness (Lane.CreateLane(1));

            // Assert
            Assert.AreEqual (57.296f, steepness, 0.001f);
        }

		[Test]
		public void StraightPieceLength()
		{
			// Arrange
			Piece piece = new Piece ();
			piece.length = 115;

			// Act
			RaceTrackPiece trackPiece = new RaceTrackPiece (piece);

			// Assert
            Assert.AreEqual (115, trackPiece.GetLength (Lane.CreateLane(0)));
		}

        [Test]
        public void GetNextSwitchFromPiece()
        {
            // Arrange
            var keimola = TestUtils.LoadKeimola();

            // Act
            var piece = keimola.CurrentTrackPiece(19);
            var nextSwitch = piece.GetNextSwitch();

            // Assert
            Assert.AreEqual(25, nextSwitch.Index);
        }

        #region Straight class tests

        [Test]
        public void StraightIsCreated()
        {
            Piece piece1 = new Piece { length = 100 };
            Piece piece2 = new Piece { length = 100 };
            Piece piece3 = new Piece { length = 100 };

            var rtPiece1 = new RaceTrackPiece(piece1);
            var rtPiece2 = new RaceTrackPiece(piece2);
            var rtPiece3 = new RaceTrackPiece(piece3);

            Straight straight = new Straight();
            straight.Add(rtPiece1);
            straight.Add(rtPiece2);
            straight.Add(rtPiece3);

            Assert.AreEqual(300, straight.TotalLength);
        }

        [Test]
        public void StraightDoesNotAcceptBends()
        {
            Piece piece1 = new Piece { length = 100 };
            Piece piece2 = new Piece { length = 100 };
            Piece piece3 = new Piece { angle = 30, radius = 100 };

            var rtPiece1 = new RaceTrackPiece(piece1);
            var rtPiece2 = new RaceTrackPiece(piece2);
            var rtPiece3 = new RaceTrackPiece(piece3);

            Straight straight = new Straight();
            straight.Add(rtPiece1);
            straight.Add(rtPiece2);
            straight.Add(rtPiece3);

            Assert.AreEqual(200, straight.TotalLength);
        }

        #endregion
	}
}

