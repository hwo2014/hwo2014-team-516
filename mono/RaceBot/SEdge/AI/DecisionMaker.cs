﻿using System;
using System.Collections.Generic;
using SEdge.Common;

namespace SEdge.AI
{
    public enum DecisionStyle
    {
        FirstActive,
        FirstOfHighest,
        Highest
    }

    public class DecisionMaker : IAiState
    {
        protected double highest = -1;
        protected DecisionStyle decisionStyle;
        protected List<IAiState> aiStates = new List<IAiState>();

        public double Value { get { return highest; } }

        public bool PassThrough { get; private set; }

        public DecisionStyle DecisionStyle
        {
            get { return decisionStyle; }
            set { decisionStyle = value; }
        }

        public void Add(IAiState aiState)
        {
            aiStates.Add(aiState);
            PassThrough = false;
        }
        
        public virtual void CalculateDecisionValue()
        {
            highest = -1;
            foreach (var aiState in aiStates)
            {
                aiState.CalculateDecisionValue();
                if (!aiState.PassThrough)
                {
                    highest = Math.Max(aiState.Value, highest);
                    if (DecisionStyle== DecisionStyle.FirstActive && highest > 0)
                        return;
                }
            }
        }


        public void Execute(IZTime zTime)
        {
            switch (DecisionStyle)
            {
                case DecisionStyle.FirstActive:
                    for (int i = 0; i < aiStates.Count; i++)
                    {
                        if (aiStates[i].Value > 0)
                        {
                            aiStates[i].Execute(zTime);
                            if (!aiStates[i].PassThrough)
                                return;
                        }
                    }
                    break;
                case DecisionStyle.FirstOfHighest:
                    
                    
                case DecisionStyle.Highest:
                    for (int i = 0; i < aiStates.Count; i++)
                    {
                        if (highest <= aiStates[i].Value)
                        {
                            aiStates[i].Execute(zTime);
                            if (!aiStates[i].PassThrough)
                                return;
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
