﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using RaceBot.Utilities;
using SEdge.AI;
using SEdge.Common;

namespace RaceBot.AI.Learning
{
    public interface IFrictionLearner
    {
        double SearchValue { get; set; }
    }
    public class FrictionForceLearner : DecisionMaker
    {
        private bool done = false;
        private double lowFrictionSearch = 500;
        private double currentFrictionSearch = 1100;
        private double highFrictionSearch = 2000;
        private RaceKnowledge knowledge;

        private double lastAngle = 0;
        private double lastAngleSpeed = 0;
        double lastCentrifugalForce = 0;

        public FrictionForceLearner(RaceKnowledge knowledge)
        {
            this.knowledge = knowledge;
        }


        public double CurrentFrictionSearch
        {
            get { return currentFrictionSearch; }
        }


        public override void CalculateDecisionValue()
        {
            if (done) return;

            
            if (EndFrictionLearner) return;
            
            var currentLane = knowledge.MyCar.PiecePosition.lane;
            
            var newAngle = knowledge.MyCar.Angle;
            if (lastCentrifugalForce + 0.0001 >= lowFrictionSearch && Math.Abs(knowledge.MyCar.VelocityDelta) < knowledge.MyCar.Acceleration)
            {
                var alpha = Math.Abs(knowledge.MyCar.TickAngleChangeSpeed + knowledge.MyCar.AngleHarmonizer);

                
                    if (alpha <= 0.000000001)
                    {
                        lowFrictionSearch = lastCentrifugalForce;
                    }
                    else if (alpha > 0.000000001)
                    {
                        highFrictionSearch = Math.Min(highFrictionSearch, lastCentrifugalForce);
                        if (lowFrictionSearch > lastCentrifugalForce)
                            lowFrictionSearch -= 3;
                    }
                    currentFrictionSearch = lowFrictionSearch + (highFrictionSearch - lowFrictionSearch) / 2;
                //}
                //else
                //{
                //    currentFrictionSearch = lowFrictionSearch;
                //}
                //if (highFrictionSearch < lowFrictionSearch)
                //{
                //    var temp = lowFrictionSearch;
                //    lowFrictionSearch = highFrictionSearch;
                //    highFrictionSearch = temp;
                //}
                foreach (var state in aiStates)
                {
                    if (state is IFrictionLearner)
                        (state as IFrictionLearner).SearchValue = currentFrictionSearch;
                }
            }

            if (knowledge.MyCar.CurrentTrackPiece.IsBend())
                lastCentrifugalForce = RaceMath.CentrifugalForce(knowledge.MyCar.CurrentVelocity,
                    knowledge.MyCar.CurrentTrackPiece.GetRadius(currentLane));
            else
                lastCentrifugalForce = 0;

            lastAngleSpeed = newAngle - lastAngle;
            lastAngle = newAngle;
            base.CalculateDecisionValue();
        }


        private bool EndFrictionLearner
        {
            get
            {
                if (Math.Abs(lowFrictionSearch - highFrictionSearch) < 0.01)
                {
                    done = true;
                    highest = -1;
                    RaceMath.Fk = (lowFrictionSearch + highFrictionSearch) / 2;
                    knowledge.Logger.WriteLine("Friction force found: " + RaceMath.Fk);
                    knowledge.EnableLearner = true;
                    return true;
                }
                return false;
            }
        }
    }
}
