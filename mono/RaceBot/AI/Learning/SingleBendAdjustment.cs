﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaceBot.AI.Learning
{
    [Serializable]
    public sealed class SingleBendAdjustment
    {
        public int BendStart;
        public int BendEnd;

        public double Adjustment = 0.0d;

        /// <summary> anything below ~57 angle is considered safe</summary>
        public double SafeBottom = -50;

        /// <summary> Lowest speed we have crashed with on this bend</summary>
        public double LowestCrash = 300;

        public double MaxSteepness = 0;

        public double LastMaxAngle = 0;

        public bool IsLeft;

        public bool IndexMatch(int pieceIndex)
        {
            // TODO: need to add consideration for laneswitches
            return pieceIndex >= BendStart && pieceIndex <= BendEnd;
        }
    }
}
