﻿using System;
using System.Configuration;
using RaceBot.Utilities;
using SEdge.Common;
using RaceBot.Messages.Send;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    public class SwitchLaneDodge : SpinPreventerPrime
    {
        private int switchingLaneFor = -1;
        private int switchingLaneForPass = -1;
        private bool passingBy;
        private int passedCarNewLane;
        private int passedCarIndex;
        private int smallestIndex = 0;
        private int pieceIndex;
        private double smallestDistance = 999;
        private bool messageReady;
        private RaceCar carPassing;

        public SwitchLaneDodge(RaceKnowledge knowledge)
            : base(knowledge, 50)
        {
        }

        public override bool PassThrough
        {
            get
            {
                return true;
            }
        }

        public override void CalculateDecisionValue()
        {
            if (Track.Lanes.Length < 2)
            {
                Value = -1;
                return;
            }

            RaceTrackPiece currentTrackPiece = MyCar.CurrentTrackPiece;

            RaceTrackPiece nextTrackPiece = currentTrackPiece.GetNext();

            var s1 = currentTrackPiece.GetNextSwitch();
            if (s1 == null)
            {
                // The track has no switches at all
                Value = -1;
                return;
            }

            var s2 = s1.GetNextSwitch();
            var piece = s1;
            double distanceBetween = 0;
            for (int i = 0; i < Track.Pieces.Count; i++)
            {
                distanceBetween += piece.GetLength(MyCar.CurrentLane);
                piece = piece.GetNext();
                if (piece.Index == s2.Index)
                    break;
            }

            //Check if a car needs to be passed and if a switch is nearby to do it
            passingBy = false;
            foreach (var car in knowledge.Cars)
            {
                if (car.Equals(MyCar)) continue;

                var distanceToNextCar = Track.DistanceToNextCar(MyCar, car);
                // if enemy is spawning in our path:
                if (car.IsOut &&
                    (car.ExpectedSpawnTime - knowledge.RaceTime.TotalD) < distanceBetween / MyCar.CurrentVelocity && distanceToNextCar < distanceBetween
                    && distanceToNextCar > -40)
                {
                    // EVADE PRIORITY 1
                    passingBy = true;
                    
                    smallestDistance = -double.NegativeInfinity;
                    passedCarNewLane = car.PiecePosition.lane.endLaneIndex;
                    carPassing = car;
                }

                if (distanceToNextCar / MyCar.CurrentVelocity < (distanceBetween - distanceToNextCar) / car.CurrentVelocity
                    && distanceToNextCar > 0 && distanceToNextCar < distanceBetween && car.AiData.IsSlowerThanMe() && !car.IsOut)
                {
                    passingBy = true;

                    if (distanceToNextCar * car.AiData.Slope < smallestDistance)
                    {
                        smallestDistance = distanceToNextCar * car.AiData.Slope;
                        passedCarNewLane = car.PiecePosition.lane.endLaneIndex;
                        carPassing = car;
                    }
                }
            }
            smallestDistance = 999;


            //Insurance to prevent trying to pass multiple times during the same piece
            /*
            if (passingBy)
            {
                if (nextTrackPiece.HasSwitch && switchingLaneFor != nextTrackPiece.Index)
                    Value = 1;
                else
                {
                    if (switchingLaneFor != nextTrackPiece.Index)
                        switchingLaneFor = -1;
                    Value = -1;
                }
                return;
            }

           if (!passingBy)
           {
               if (nextTrackPiece.HasSwitch && switchingLaneFor != nextTrackPiece.Index)
               {
                   Value = 1;
               }
               else
               {
                   if (switchingLaneFor != nextTrackPiece.Index)
                       switchingLaneFor = -1;
                   Value = -1;
               }
           }*/

            //Only check for lane switches at the last possible moment to ensure other cars have made their choices

            //if (MyCar.CurrentTrackPiece.LastTickOnPiece(MyCar) && switchingLaneFor != nextTrackPiece.Index && nextTrackPiece.HasSwitch)
            if (MyCar.CurrentTrackPiece.LastTickOnPiece(MyCar) && switchingLaneFor != nextTrackPiece.Index)
            {
                Value = 1;
            }
            else
            {
                if (switchingLaneFor != nextTrackPiece.Index)
                    switchingLaneFor = -1;
                Value = -1;
            }
        }

        public override void Execute(IZTime zTime)
        {
            // Already going to switch lane at the next switch. Only pass this if wanting to pass a car
            //if (knowledge.ShouldSwitchLane())
            //{
            //    return;
            //}
            
            var last = knowledge.SwitchDirection;
            //The optimal route. This is skipped if we want to pass a car
            if (!passingBy)
            {
                int optimumLane = Track.LaneSolver.GetOptimumLane(MyCar.PiecePosition);

                int currentLane = MyCar.CurrentLane.endLaneIndex;

                if (currentLane < optimumLane)
                {
                    knowledge.SwitchDirection = SwitchDirection.Right;          
                }
                else if (currentLane > optimumLane)
                {
                    knowledge.SwitchDirection = SwitchDirection.Left;
                }
                else
                {
                    knowledge.SwitchDirection = SwitchDirection.None;
                }
            }

            //Check to see if we want to pass by a slower car
            if (passingBy)
            {

                int currentLane = MyCar.CurrentLane.endLaneIndex;

                var fastLanes = Track.LaneSolver.GetFastLanes(MyCar.PiecePosition);
                
                //Choose the lane depending on the slower car's choices
                if (passedCarNewLane == currentLane)
                {
                    var bestLane = fastLanes[0];
                    var secondBest = fastLanes[1];
                    
                    if (bestLane.Index == passedCarNewLane)
                    {
                        var k = 3 * (bestLane.Distance - secondBest.Distance) / (bestLane.Distance + secondBest.Distance);
                        if (carPassing.AiData.Slope > k || carPassing.IsOut) //isout means it is spawning right up
                        {
                            bestLane = fastLanes[1];
                        }

                    }
                    // random function to generate slope    
                    
                    if (currentLane < bestLane.Index)
                    {
                        knowledge.SwitchDirection = SwitchDirection.Right;
                        switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
                    }
                    else if (currentLane > bestLane.Index)
                    {
                        knowledge.SwitchDirection = SwitchDirection.Left;
                        switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
                    }
                    else
                    {
                        knowledge.SwitchDirection = SwitchDirection.None;
                    }
                }
                else
                {
                    knowledge.SwitchDirection = SwitchDirection.None; ;
                }
            }
            //if (MyCar.IsTurboActive)
            //    knowledge.SwitchDirection = SwitchDirection.None;

            if (WillCrashOnSpeed())
            {
                knowledge.SwitchDirection = last;
            }
            else
            {
                switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
            }

            if (last != knowledge.SwitchDirection)
            {
                knowledge.Logger.WriteLine("Decided to switch to: " + knowledge.SwitchDirection);
            }
        }
    }
}