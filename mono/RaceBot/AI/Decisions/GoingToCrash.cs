﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class GoingToCrash : SpinPreventerPrime
    {
        private int d0;
        private int d1;
        private double maxAngleonZero;


        public GoingToCrash(RaceKnowledge knowledge, int predictTicks)
            : base(knowledge, predictTicks)
        {
        }


        public override void CalculateDecisionValue()
        {
            d0 = 9998;
            d1 = 9999;
            endOfTurnDistance = 0;
            Throttle = 0;

            if (WillCrashOnSpeed(false, 60))
            {
                d0 = ticksTillMax;
            }
            maxAngleonZero = maxAngle;
            endOfTurnDistance = 1;
            Throttle = 1;
            if (WillCrashOnSpeed(false, 60))
            {
                d1 = ticksTillMax;
            }
            Value = 0.0000000001;
        }


        public override void Execute(IZTime zTime)
        {
            //if ((maxAngle > 0 && MyCar.CurrentTrackPiece.IsLeftTurn()) || (maxAngle < 0 && MyCar.CurrentTrackPiece.IsRightTurn()))
            //    knowledge.Throttle = 1;
            //else
            if (d0 > d1)
                knowledge.Throttle = 0;
            else
            {
                if (d1 > d0 || Math.Abs(maxAngle) <=Math.Abs(maxAngleonZero))
                    knowledge.Throttle = 1;
                else
                {
                    knowledge.Throttle = 0;
                }
            }
                
            //knowledge.Throttle = Math.Max(Math.Min((endOfTurnDistance + 10) / 200, 1), 0);
            Log(zTime, "Crsh", "a0: " + maxAngleonZero + " a1: " + maxAngle + " d0: " +d0 + " d1: " + d1);
        }
    }
}
