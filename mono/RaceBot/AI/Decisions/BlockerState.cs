﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Send;
using RaceBot.Race;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class BlockerState :BaseDecision
    {
        private int switchingLaneFor = -1;
        private int optimumLane;
        public BlockerState(RaceKnowledge knowledge) : base(knowledge)
        {
        }


        public override bool PassThrough
        {
            get { return true; }
        }

        public override void CalculateDecisionValue()
        {
            // Already going to switch lane at the next switch
            if (!ShouldSwitchLane())
            {
                Value = -1;
                return;
            }

            RaceTrackPiece nextTrackPiece = MyCar.CurrentTrackPiece.GetNext();

            if (nextTrackPiece.HasSwitch && switchingLaneFor != nextTrackPiece.Index)
            {
                Value = 1;
            }
            else
            {
                if (switchingLaneFor != nextTrackPiece.Index)
                    switchingLaneFor = -1;
                Value = -1;
            }
        }


        private bool ShouldSwitchLane()
        {
            double smallestdistance = double.MaxValue;
            int index = -1;
            for (int i = 0; i < knowledge.Cars.Count(); i++)
            {
                var car = knowledge.Cars[i];
                if (car.Equals(MyCar) || car.IsOut) continue;
                var d = Track.DistanceToNextCar(car, MyCar);
                if (d > 0 && d < smallestdistance)
                {
                    smallestdistance = d;
                    index = i;
                }
            }
            if (index < 0)
                return false;

            optimumLane = knowledge.Cars[index].PiecePosition.lane.endLaneIndex;
            return true;
        }


        public override void Execute(IZTime zTime)
        {
            // Already going to switch lane at the next switch
            if (knowledge.ShouldSwitchLane())
            {
                return;
            }

            var currentLane = MyCar.CurrentLane.endLaneIndex;

            if (currentLane < optimumLane)
            {
                knowledge.SwitchDirection = SwitchDirection.Right;
                switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
            }
            else if (currentLane > optimumLane)
            {
                knowledge.SwitchDirection = SwitchDirection.Left;
                switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
            }
            else
            {
                knowledge.SwitchDirection = SwitchDirection.None;
            }
        }
    }
}
