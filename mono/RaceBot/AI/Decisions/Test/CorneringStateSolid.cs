﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.AI.Learning;
using RaceBot.Utilities;
using SEdge.Common;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    public class CorneringStateSolid : CorneringStatePrime, IFrictionLearner
    {
        public bool BasedOnFk = false;
        public double SearchValue
        {
            get { return bendForce; }
            set { bendForce = value; } 
        }

        public CorneringStateSolid(RaceKnowledge knowledge, double fc) : base(knowledge, fc)
        {
            logName = "CrnS";
        }


        public override bool PassThrough
        {
            get { return false; }
        }


        public override void CalculateDecisionValue()
        {
            var currentPiece = MyCar.CurrentTrackPiece;
            RaceTrackPiece nextBend = Track.NextOrCurrentTurn (currentPiece.Index);
            var steepness = nextBend.GetBendSteepness(knowledge.PredictLane(nextBend.Index));

            double distance = CalculateBrakeDistance(steepness, nextBend.GetIndex(), currentPiece.Index, false);

            if (BasedOnFk)
            {
                desiredVelocity = Math.Sqrt((bendForce +RaceMath.Fk) * nextBend.GetRadius(MyCar.PiecePosition.lane));
            }
            else
            {
                desiredVelocity = Math.Sqrt(bendForce * nextBend.GetRadius(MyCar.PiecePosition.lane));
            }
            

            //for (ticks = 0; ticks < 50; ticks++)
            //{
            //    brakingDistance += velocity * RaceMath.TickDuration;
            //    velocity = RaceMath.VelocityOnNextTick(velocity);
            //    // add one extra tick to calculation for super late braking optimized to subtick
            //    if (velocity <= desiredVelocity)
            //        break;
            //}

            double velocity;
            var brakingDistance = PredictBrakingDistance(out velocity);

            if (distance <= 0 || distance < brakingDistance + velocity * RaceMath.TickDuration)
            {
                Value = 1; // activate
            }
            else
                Value = -1; // deactivate
        }


        protected double CalculateBrakeDistance(double steepness, int nextBendIndex, int myCarOnPiece, bool log)
        {


            var distance = Track.DistanceToPiece(nextBendIndex, MyCar.PiecePosition);

            if (distance > 0 || steepness != lastSteepness)
            {
                lastSteepness = steepness;
                steepness = Math.Abs(steepness);
                // variable to determinen if our car is already in a tough spot
                //var carAngleReduction = Math.Sign(knowledge.Track.Pieces[nextBendIndex].Angle)
                //                        * (MyCar.AngleSpeed) * 0.025 - Math.Abs(MyCar.Angle) * 0.1;

                desiredVelocity = knowledge.BendsData.GetBendValues(steepness).DesiredVelocity;
                lateBrakeDistance = knowledge.BendsData.GetBendValues(steepness).LateBrakeDistance;
          
                if (desiredVelocity < 1) desiredVelocity = 1;

                if (knowledge.EnableLearner)
                    desiredVelocity += knowledge.BendLearner.GetBendAdjustment(nextBendIndex);

                //if (log)
                //    Console.WriteLine(string.Format("Reduction: {0} CAngle: {1} CAngSpd: {2} dVelocity: {3}",
                //        ZLib.FloatToText(carAngleReduction),
                //        ZLib.FloatToText(MyCar.Angle), ZLib.FloatToText(MyCar.AngleSpeed),
                //        ZLib.FloatToText(desiredVelocity)));
            }
            else
                steepness = Math.Abs(steepness);
            return distance;
        }
        
    }
}
