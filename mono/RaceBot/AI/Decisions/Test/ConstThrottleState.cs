﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEdge.Common;

namespace RaceBot.AI.Decisions.Test
{
    public class ConstThrottleState : BaseDecision
    {
        private readonly double throttle;


        public ConstThrottleState(RaceKnowledge knowledge, double throttle) : base(knowledge)
        {
            this.throttle = throttle;
        }


        public override bool PassThrough
        {
            get { return false; }
        }

        public override void CalculateDecisionValue()
        {
            Value = 1;
        }


        public override void Execute(IZTime zTime)
        {
            knowledge.Throttle = throttle;
            Log(zTime, "Cnst");
        }
    }
}
