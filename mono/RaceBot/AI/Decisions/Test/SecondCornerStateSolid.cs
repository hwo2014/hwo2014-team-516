﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.AI.Learning;
using RaceBot.Utilities;
using SEdge.Common;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    [Obsolete("Will get to an infinite loop on a track with no straight pieces. You have been warned!")]
    public class SecondCornerStateSolid : CorneringStatePrime, IFrictionLearner
    {
        public bool BasedOnFk = false;

        public double SearchValue
        {
            get { return bendForce; }
            set { bendForce = value; }
        }

        public SecondCornerStateSolid(RaceKnowledge knowledge, double fc) : base(knowledge, fc)
        {
            logName = "Crn2";
        }


        public override bool PassThrough
        {
            get { return false; }
        }

        public override void CalculateDecisionValue()
        {
            Value = -1; // deactivate
            var myCarOnPiece = MyCar.PiecePosition.pieceIndex;
            var nextBend = Track.NextOrCurrentTurn(myCarOnPiece);
            for (int i = 0; i < 4; i++)
            {
                
                RaceTrackPiece secondBend = nextBend;
                var predictLane = knowledge.PredictLane(nextBend.Index);
                var previousSteepness = nextBend.GetBendSteepness(predictLane);
                while (Math.Abs(previousSteepness -
                    secondBend.GetBendSteepness(predictLane)) < 0.0001 ||
                    secondBend.IsStraight())
                {
                    secondBend = secondBend.GetNext();
                    if (secondBend.IsStraight())
                        previousSteepness = 0;
                }

                var steepness = secondBend.GetBendSteepness(predictLane);
                //if (Math.Abs(steepness) >
                //    Math.Abs(secondBend.GetBendSteepness(distanceFromCenter)))
                //{
                //    Value = -1;
                //    return;
                //}
                if (BasedOnFk)
                {
                    desiredVelocity = Math.Sqrt((bendForce + RaceMath.Fk) * secondBend.GetRadius(MyCar.PiecePosition.lane));
                }
                else
                {
                    desiredVelocity = Math.Sqrt(bendForce * secondBend.GetRadius(MyCar.PiecePosition.lane));
                }

                double distance = GetBrakeDistance(steepness, secondBend, myCarOnPiece);


                var velocity = MyCar.CurrentVelocity;
                var brakingDistance = PredictBrakingDistance(out velocity);

                if (distance < brakingDistance + velocity * RaceMath.TickDuration &&
                    MyCar.CurrentVelocity >= desiredVelocity)
                {
                    //Console.WriteLine("Prepare second corner steepness: " + ZLib.FloatToText(steepness));
                    Value = 1; // activate
                    return;
                }
                nextBend = secondBend;
            }   
        }


        protected override double GetBrakeDistance(double steepness, RaceTrackPiece nextBend, int myCarOnPiece)
        {


            var distance = Track.DistanceToPiece(nextBend.Index, MyCar.PiecePosition);

            if (distance > 0 || steepness != lastSteepness)
            {
                lastSteepness = steepness;
                steepness = Math.Abs(steepness);
                // variable to determinen if our car is already in a tough spot
                //var carAngleReduction = Math.Sign(knowledge.Track.Pieces[nextBendIndex].Angle)
                //                        * (MyCar.AngleSpeed) * 0.025 - Math.Abs(MyCar.Angle) * 0.1;

                //desiredVelocity = knowledge.BendsData.GetBendValues(steepness).DesiredVelocity;
                lateBrakeDistance = 0;

                if (desiredVelocity < 1) desiredVelocity = 1;

                //if (knowledge.EnableLearner)
                //    desiredVelocity += knowledge.BendLearner.GetBendAdjustment(nextBendIndex);

                //if (log)
                //    Console.WriteLine(string.Format("Reduction: {0} CAngle: {1} CAngSpd: {2} dVelocity: {3}",
                //        ZLib.FloatToText(carAngleReduction),
                //        ZLib.FloatToText(MyCar.Angle), ZLib.FloatToText(MyCar.AngleSpeed),
                //        ZLib.FloatToText(desiredVelocity)));
            }
            else
                steepness = Math.Abs(steepness);
            return distance;
        }
    }
}
