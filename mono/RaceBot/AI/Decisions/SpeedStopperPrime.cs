﻿using System;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class SpeedStopperPrime : SpinPreventerPrime
    {
        private double throttle;
        //private double throttle1;
        private int ticks;
        protected int ticksOnFullSpeed;
        private int ticksIncorner = 0;
        protected override double Throttle
        {
            get
            {
                ticks--;
                if (ticks < 0)
                    return 0;
                return throttle;
                
            }
            set
            {
                //throttle = value;
                ticks = ticksOnFullSpeed;
            }
        }


        public SpeedStopperPrime(RaceKnowledge knowledge, int predictTicks, int ticksOnFullSpeed, SpinMother spinMother)
            : base(knowledge, predictTicks)
        {
            this.ticksOnFullSpeed = ticksOnFullSpeed;
            this.throttle = 1;
            this.spinMother = spinMother;
        }


        public override void CalculateDecisionValue()
        {
            if (spinMother != null )
            {
                if (MyCar.CurrentTrackPiece.IsBend() || Math.Abs(MyCar.VelocityDelta) > 50
                    || MyCar.CurrentVelocity < 200)
                {
                    spinMother.BreakingToCorner = false;
                }
            }

            var currentTrackPiece = knowledge.MyCar.CurrentTrackPiece;

            if (currentTrackPiece.IsBend())
            {
                //ticksIncorner ++;
                //if (ticksIncorner > 3)
                //{
                //    Value = 0;
                Value = 0;
                    return;
                //}
            }
            else
            {
                ticksIncorner = 0;
            }

            Throttle = throttle;
            if (WillCrashOnSpeed(exitWhenStraigth: false))
            {
                Value = 0;
            }
            else
            {
                Value = 1;
            }
        }

        public override void Execute(IZTime zTime)
        {
            int bonusTicks = 0;
            if (spinMother != null  && spinMother.BreakingToCorner)
            {
                Value = 0;
            }
            else
            {
                //var bend = MyCar.CurrentTrackPiece;
                //for (int i = 0; i < 20; i++)
                //{
                //    if (bend.IsBend())
                //        break;
                //    bend = bend.GetNext();
                //}

                //var y = knowledge.BendLearner.GetBendAdjustmentPiece(bend.Index).BendEnd;
                ////var d = bend.GetLength(knowledge.PredictLane(bend.Index));
                ////var piece = bend.GetNext();
                //double d = 0;
                //while (y > bend.Index)
                //{
                //    d += bend.GetLength(knowledge.PredictLane(bend.Index));
                //    bend = bend.GetNext();
                //}
                //bonusTicks = (int) (d / 100);

                var predictLane = knowledge.PredictLane(MyCar.CurrentTrackPiece.GetNext().Index);
                //if (ticksOnFullSpeed > (predictLane.startLaneIndex != predictLane.endLaneIndex && MyCar.CurrentTrackPiece.GetNext().IsBend() ? 1 : 0))
                if (ticksOnFullSpeed > 0)
                {
                    knowledge.Throttle = throttle;
                }
                else
                {
                    knowledge.Throttle = 0;
                    spinMother.BreakingToCorner = true;
                }
            }
            Log(zTime, "S" + ticksOnFullSpeed, " " + ZLib.FloatToText(maxAngle) + " t" + ticksTillMax + " b"+bonusTicks);
        }
    }
}