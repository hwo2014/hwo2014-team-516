﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Utilities;
using SEdge.Common;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    public class CorneringState : BaseDecision
    {
        protected double desiredVelocity = 370;
        protected double lastSteepness = 0;
        protected double lateBrakeDistance;
        protected string logName;
        

        public CorneringState(RaceKnowledge knowledge) : base(knowledge)
        {
            logName = "Crnr";
        }


        public override bool PassThrough
        {
            get { return false; }
        }


        public override void CalculateDecisionValue()
        {
			var myCarOnPiece = MyCar.PiecePosition.pieceIndex;
            RaceTrackPiece nextBend = Track.NextOrCurrentTurn (myCarOnPiece);

            var steepness = nextBend.GetBendSteepness(MyCar.CurrentLane);

            double distance = CalculateBrakeDistance(steepness, nextBend.GetIndex(), myCarOnPiece);
            var velocity = MyCar.CurrentVelocity;
            int ticks;
            double brakingDistance = 0;
            for (ticks = 0; ticks < 50; ticks++)
            {
                brakingDistance += velocity * RaceMath.TickDuration;
                velocity = RaceMath.VelocityOnNextTick(velocity);
                // add one extra tick to calculation for super late braking optimized to subtick
                if (velocity <= desiredVelocity)
                    break;
            }

            
            //var brakingDistance = RaceMath.BreakingDistance(
            //    RaceMath.GetTickFromVelocity(MyCar.CurrentVelocity),
            //    RaceMath.GetTickFromVelocity(desiredVelocity)) - lateBrakeDistance;

            if (distance <= 0 || distance < brakingDistance - lateBrakeDistance)
            {
                Value = 1; // activate
            }
            else
                Value = -1; // deactivate
                
        }


        protected virtual double CalculateBrakeDistance(double steepness, int nextBendIndex, int myCarOnPiece)
        {


            var distance = Track.DistanceToPiece(nextBendIndex, MyCar.PiecePosition);

            if (distance > 0 || steepness != lastSteepness)
            {
                lastSteepness = steepness;
                steepness = Math.Abs(steepness);
                // variable to determinen if our car is already in a tough spot
                //var carAngleReduction = Math.Sign(knowledge.Track.Pieces[nextBendIndex].Angle)
                //                        * (MyCar.AngleSpeed) * 0.025 - Math.Abs(MyCar.Angle) * 0.1;

                desiredVelocity = knowledge.BendsData.GetBendValues(steepness).DesiredVelocity;
                lateBrakeDistance = knowledge.BendsData.GetBendValues(steepness).LateBrakeDistance;
          
                if (desiredVelocity < 1) desiredVelocity = 1;

                if (knowledge.EnableLearner)
                    desiredVelocity += knowledge.BendLearner.GetBendAdjustment(nextBendIndex);

                //if (log)
                //    Console.WriteLine(string.Format("Reduction: {0} CAngle: {1} CAngSpd: {2} dVelocity: {3}",
                //        ZLib.FloatToText(carAngleReduction),
                //        ZLib.FloatToText(MyCar.Angle), ZLib.FloatToText(MyCar.AngleSpeed),
                //        ZLib.FloatToText(desiredVelocity)));
            }
            else
                steepness = Math.Abs(steepness);
            return distance;
        }


        public override void Execute(IZTime zTime)
        {
            double desiredSpeedDelta;
            
            desiredSpeedDelta = desiredVelocity - MyCar.CurrentVelocity;
            

            desiredSpeedDelta -= // remove drag from air resistance
                RaceMath.VelocityOnNextTick(MyCar.CurrentVelocity) -
                MyCar.CurrentVelocity;

            // Slightly reduces desired speed based on the current car angle
            //var throttleEase = Math.Cos(MyCar.Angle / 180 * Math.PI);
            knowledge.Throttle = Math.Min(Math.Max(desiredSpeedDelta / MyCar.Acceleration, 0), 1);

            Log(zTime, logName);
        }
    }
}
