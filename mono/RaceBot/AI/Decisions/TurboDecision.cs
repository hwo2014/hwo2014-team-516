﻿using System;
using RaceBot.AI.Decisions;
using RaceBot.AI;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot
{
    public class TurboDecision : SpeedStopperPrime
    {
        /// <summary>
        /// Returns true if the throttle is forced, and false otherwise.
        /// </summary>
        public static bool ForcedThrottle { get; private set; }

        private Straight bestStraight;

        public override bool PassThrough
        {
            get
            {
                return false;
            }
        }

        public TurboDecision(RaceKnowledge knowledge) : base(knowledge, 75, 15, null)
        {
            ForcedThrottle = false;

            
        }


        private void AnalyzeStraights(RaceKnowledge knowledge)
        {
            double longestD = 0;
            foreach (var straight in knowledge.Track.Straights)
            {
                var pos = new PiecePosition() {pieceIndex = straight.StartsAt.Index, lane = new Lane()};
                var a = 0.0;
                var ad = 0.0;
                var v = 350.0;
                var alpha = 0.0;
                var piece = straight.StartsAt;
                var d = 0.0;
                for (int i = 0; i < 200; i++)
                {
                    alpha = RaceMath.AngleAccelOnNextTick(a, ad, v, piece.GetRadius(pos.lane),
                        piece.IsLeftTurn());
                    ad += alpha;
                    a += ad;
                    var multiplier = 1;
                    if (i > 30)
                        multiplier = 3;
                    v = RaceMath.VelocityOnNextTick(v) + RaceMath.Acceleration * multiplier;
                    d += v * RaceMath.TickDuration;
                    pos.inPieceDistance += v * RaceMath.TickDuration;
                    if (pos.inPieceDistance > piece.GetLength(pos.lane))
                    {
                        pos.inPieceDistance -= piece.GetLength(pos.lane);
                        piece = piece.GetNext();
                        pos.pieceIndex = piece.Index;
                        //if (predictLane.endLaneIndex != predictLane.startLaneIndex && crashAngle != 60)
                        //    crashAngle = 59;
                    }
                    if (a < -RaceMath.CrashAngle || a > RaceMath.CrashAngle)
                        break;
                }

                if (d > longestD)
                {
                    longestD = d;
                    bestStraight = straight;
                }
            }
        }


        public static void ResetForcedThrottle()
        {
            ForcedThrottle = false;
        }

        public override void CalculateDecisionValue()
        {
            if (RaceMath.FkFound && bestStraight == null)
                AnalyzeStraights(knowledge);

            Value = (ShouldTurbo() ? 1 : -1);
        }

        private bool ShouldTurbo()
        {
            if (ForcedThrottle)
                return true;

            // 1st priority is to force throttle 1.0 and to
            // use any remaining turbos if the race/qualifying is just about to finish
            if ((!knowledge.IsQualifyingPeriod && MyCar.IsOnLastLap) 
                || (knowledge.IsQualifyingPeriod && knowledge.QualifyingTimeLeft < (MyCar.BestLapTime - 2000)))
            {
                bool force = true;
                var piece = MyCar.CurrentTrackPiece;
                for (int i = 0; i < knowledge.Track.Pieces.Count - piece.Index; i++)
                {
                    if (piece.IsBend())
                    {
                        force = false;
                        break;
                    }
                    piece = piece.GetNext();
                }
                if (force)
                {
                    knowledge.Logger.WriteLine(MyCar.carId.name + ": Last straight of last lap! Using turbo!");
                    ForcedThrottle = true;
                    knowledge.Logger.WriteLine("Forced throttle enabled!");
                    return true;
                }
            }

            long ticksUntilNextTurbo = knowledge.NextTurboOnTick - knowledge.RaceTime.Ticks;
            double distanceToNextTurn = Track.DistanceToCurrentOrNextBend(MyCar.PiecePosition.pieceIndex, MyCar.PiecePosition.inPieceDistance);

            if(MyCar.IsTurboAvailable && !MyCar.IsTurboActive)
            {
                Throttle = MyCar.AvailableTurbo.Factor;
                if (WillCrashOnSpeed(false))
                {
                    return false;
                }

                //if (bestStraight != null && !MyCar.IsSwitchingLanes() && MyCar.PiecePosition.pieceIndex == bestStraight.StartsAt.Index - 1 &&
                //    MyCar.CurrentTrackPiece.LastTickOnPiece(MyCar))
                //{
                //    knowledge.Logger.WriteLine(MyCar.carId.name + ": Last tick before main straight. WRUUUM!");
                //    return true;
                //}

                

                // 2nd priority is to use turbo on the main straight
                if (bestStraight != null &&
                    MyCar.PiecePosition.pieceIndex >= bestStraight.StartsAt.Index - 2 &&
                    MyCar.PiecePosition.pieceIndex <= bestStraight.StartsAt.Index)
                {
                    knowledge.Logger.WriteLine(MyCar.carId.name + ": On main straight. Using turbo!");
                    return true;
                }

                // If we are soon to get another turbo, use current one fast because turbos do not stack
                if (ticksUntilNextTurbo < 200)
                {
                    var nextTurn = Track.NextOrCurrentTurn(MyCar.CurrentTrackPiece.Index);
                    var nextTurnSteepness = Math.Abs(nextTurn.GetBendSteepness(MyCar.CurrentLane));

                    if (distanceToNextTurn >= 200)
                    {
                        knowledge.Logger.WriteLine(MyCar.carId.name + ": Ticks until next turbo: " + ticksUntilNextTurbo +
                            ". Distance to next turn " + distanceToNextTurn + ". Using turbo!");
                        return true;
                    }
                    else if (distanceToNextTurn >= 100 && nextTurnSteepness < 50)
                    {
                        knowledge.Logger.WriteLine(MyCar.carId.name + ": Ticks until next turbo: " + ticksUntilNextTurbo +
                            ". Distance to next turn " + distanceToNextTurn + " and next turn steepness " + nextTurnSteepness + ". Using turbo!");
                        return true;
                    }
                }

                return false;
            }

            return false;
        }

        public override void Execute(IZTime zTime)
        {
            if (knowledge.MyCar.IsTurboActive || !knowledge.MyCar.IsTurboAvailable || knowledge.LastSentThrottle < 1)
            {
                knowledge.Throttle = 1;
                Log(zTime, "Trbo");
            }
            else
            {
                knowledge.ActivateTurbo();
                Log(zTime, "Trbo", " TURBO!");    
            }
                
            
        }
    }
}

