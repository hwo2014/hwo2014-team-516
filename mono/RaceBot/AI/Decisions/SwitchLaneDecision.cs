﻿using System;
using RaceBot.Messages.Receive;
using SEdge.Common;
using RaceBot.Messages.Send;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    public class SwitchLaneDecision : BaseDecision
    {
        private int switchingLaneFor = -1;

        Random rnd;

        public SwitchLaneDecision(RaceKnowledge knowledge)
            : base(knowledge)
        {
            
            rnd = new Random();
        }


        public override bool PassThrough
        {
            get
            {
                return true;
            }
        }

        public override void CalculateDecisionValue()
        {
            // Already going to switch lane at the next switch
            //if (knowledge.ShouldSwitchLane())
            //{
            //    Value = -1;
            //    return;
            //}

            RaceTrackPiece nextSwitch = MyCar.CurrentTrackPiece.GetNextSwitch();
            if (nextSwitch == null)
            {
                // No switches on this track
                Value = -1;
                return;
            }
            
            if (switchingLaneFor != nextSwitch.Index && switchingLaneFor != MyCar.CurrentTrackPiece.Index)
            {
                Value = 1;
                switchingLaneFor = nextSwitch.Index;
            }
            else
            {
                if (switchingLaneFor != nextSwitch.Index)
                    switchingLaneFor = -1;
                Value = -1;
            }
        }

        public override void Execute(IZTime zTime)
        {
            // Already going to switch lane at the next switch
            //if (knowledge.ShouldSwitchLane())
            //{
            //    return;
            //}
            var piece = MyCar.CurrentTrackPiece.GetNextSwitch().GetPrevious();
            var position = new PiecePosition()
                           {
                               inPieceDistance = piece.GetLength(MyCar.CurrentLane),
                               lane = MyCar.CurrentLane,
                               lap = MyCar.PiecePosition.lap,
                               pieceIndex = piece.Index
                           };
            int optimumLane = Track.LaneSolver.GetOptimumLane(position);
            int currentLane = MyCar.CurrentLane.endLaneIndex;

            optimumLane = (int) (rnd.NextDouble() * Track.Lanes.Length);
            if (currentLane < optimumLane)
            {
                knowledge.SwitchDirection = SwitchDirection.Right;
                
            }
            else if (currentLane > optimumLane)
            {
                knowledge.SwitchDirection = SwitchDirection.Left;
                
            }
            else
            {
                knowledge.SwitchDirection = SwitchDirection.None;
                
            }
            Log(zTime, "swtc", " " + knowledge.SwitchDirection);
        }
    }
}