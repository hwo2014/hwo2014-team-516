﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RaceBot.AI;
using SEdge.Common;

namespace RaceBot.Utilities
{
    public enum LogMode
    {
        None,
        Normal,
        FcData
    }
    public class RaceLogger
    {
        private RaceKnowledge knowledge;
        private readonly bool shortLog;
        private readonly bool longLog;
        private readonly string botName;
        private StreamWriter fileShortLog;
        private StreamWriter fileLongLog;
        private double lastSteepness = double.MaxValue;
        //private int lastT;
        private double raceSectionMinVelocity;
        private double raceSectionMaxVelocity;
        private double raceSectionMinAngle;
        private double raceSectionMaxAngle;
        private int lap = 1;

        private bool writeAllToFile;

        public LogMode LogMode
        {
            get { return LogMode.Normal; }
        }

        public string LogText { get; set; }

        public long DecisionTimeTaken { get; set; }

        public RaceLogger(RaceKnowledge knowledge, bool shortLog, bool longLog, string botName, bool writeAllToFile = false)
        {
            this.knowledge = knowledge;
            this.shortLog = shortLog;
            this.longLog = longLog;
            this.botName = botName;
            this.writeAllToFile = writeAllToFile;
        }


        public void StartLog()
        {
            try
            {
                var now = DateTime.Now;
                if (!Directory.Exists("logs"))
                    Directory.CreateDirectory("logs");

                if (shortLog)
                {
                    string filename =
                        String.Format("logs" + System.IO.Path.DirectorySeparatorChar + botName + "_rd _{0}_{1}.{2}.{3}.txt",
                            ZLib.DateToGeneralText(now),
                            now.Hour,
                            ZLib.AddNulls(now.Minute.ToString()),
                            ZLib.AddNulls(now.Second.ToString()));

                    fileShortLog = new StreamWriter(filename);
                }

                if (longLog)
                {
                    string filename =
                        String.Format("logs" + System.IO.Path.DirectorySeparatorChar + botName + "_{4}_{0}_{1}.{2}.{3}.txt",
                            ZLib.DateToGeneralText(now),
                            now.Hour,
                            ZLib.AddNulls(now.Minute.ToString()),
                            ZLib.AddNulls(now.Second.ToString()),
                            knowledge.Track.Name);

                    fileLongLog = new StreamWriter(filename);
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }


        public void Log()
        {
            if (!string.IsNullOrEmpty(LogText))
            {
                Console.WriteLine(LogText);
                knowledge.Logger.WriteLongLogData(LogText);    
            }
        }

        public void WriteLine()
        {
            WriteLine(string.Empty);
        }

        public void WriteLine(Object obj)
        {
            if (obj == null)
                return;

            WriteLine(obj.ToString());
        }

        public void WriteLine(string line)
        {
            Console.WriteLine(line);
            if (writeAllToFile)
            {
                knowledge.Logger.WriteLongLogData(line);
            }
        }

        public void Update(IZTime zTime)
        {
            var newSteepness = knowledge.MyCar.CurrentTrackPiece.GetBendSteepness(knowledge.MyCar.CurrentLane);
            if (Math.Abs(lastSteepness - newSteepness) > 0.01)
            {
                //write section data
                WriteSectionData(zTime);

                lastSteepness = newSteepness;

                raceSectionMinVelocity = knowledge.MyCar.CurrentVelocity;
                raceSectionMaxVelocity = knowledge.MyCar.CurrentVelocity;
                raceSectionMinAngle = knowledge.MyCar.Angle;
                raceSectionMaxAngle = knowledge.MyCar.Angle;
            }
            else
            {
                // update any data we can get
                raceSectionMinVelocity = Math.Min(knowledge.MyCar.CurrentVelocity, raceSectionMinVelocity);
                raceSectionMaxVelocity = Math.Max(knowledge.MyCar.CurrentVelocity, raceSectionMaxVelocity);
                raceSectionMinAngle = Math.Min(knowledge.MyCar.Angle, raceSectionMinAngle);
                raceSectionMaxAngle = Math.Max(knowledge.MyCar.Angle, raceSectionMaxAngle);
            }
        }


        public void WriteSectionData(IZTime zTime)
        {
            if (lastSteepness >= double.MaxValue) return;
            if (fileShortLog == null) return;

            // TODO: take lane switching into account
            fileShortLog.WriteLine(String.Format("Time: {0} | Lap: {1} | Piece: {2} | Steepness: {3} | Section Length: {4} | " +
                                         "Car Speed: {5} | Car min Speed: {6} | Car max Speed: {7} | " +
                                         "Car Angle: {8} | Car Min Angle: {9} | Car Max Angle: {10} |",
                ZLib.FloatToText(zTime.TotalD), lap, knowledge.MyCar.CurrentTrackPiece.GetPrevious().Index, 
                ZLib.FloatToText(lastSteepness), ZLib.FloatToText(CalculateSectionLength()),
                ZLib.FloatToText(knowledge.MyCar.CurrentVelocity), ZLib.FloatToText(raceSectionMinVelocity),
                ZLib.FloatToText(raceSectionMaxVelocity),
                ZLib.FloatToText(knowledge.MyCar.Angle), ZLib.FloatToText(raceSectionMinAngle),
                ZLib.FloatToText(raceSectionMaxAngle)));
        }

        public double CalculateSectionLength()
        {
            var trackPiece = knowledge.MyCar.CurrentTrackPiece.GetPrevious();
            double length = 0;
            while (true)
            {
                var lane = knowledge.MyCar.CurrentLane;
                var newSteepness = trackPiece.GetBendSteepness (lane);

                if (Math.Abs(lastSteepness - newSteepness) > 0.01)
                    break;
                
                length += trackPiece.GetLength(lane);
                trackPiece = trackPiece.GetPrevious ();
            }
            return length;
        }


        public void LapFinished()
        {
            lap++;
        }

        public void ReportCrash()
        {
            if (fileShortLog == null) return;

            var myCar = knowledge.MyCar;
            fileShortLog.WriteLine("---------CRASH---------");
            fileShortLog.WriteLine("Velocity: " + myCar.CurrentVelocity);
            fileShortLog.WriteLine("Angle: " + myCar.Angle);
            var lane = knowledge.MyCar.CurrentLane;
            fileShortLog.WriteLine("Steepness: " + knowledge.MyCar.CurrentTrackPiece.GetBendSteepness(lane));
            fileShortLog.WriteLine("Piece: " + myCar.PiecePosition.pieceIndex);
            fileShortLog.WriteLine("In piece position: " + myCar.PiecePosition.inPieceDistance);
            fileShortLog.WriteLine("Start Lane: " + myCar.PiecePosition.lane.startLaneIndex);
            fileShortLog.WriteLine("---------CRASH---------");
            fileShortLog.Flush();
        }


        public void CloseLog()
        {
            var RaceValues = string.Format("Accel: {0} Arsx: {1} Fk: {2} Crash angle: {3}", RaceMath.Acceleration, RaceMath.Arsx, RaceMath.Fk, RaceMath.CrashAngle);
            Console.WriteLine(RaceValues);
            
            if (fileShortLog != null)
                fileShortLog.Close();
            if (fileLongLog != null)
            {
                fileLongLog.WriteLine(RaceValues);
                fileLongLog.Close();
            }
                
        }


        public void WriteLongLogData(string text)
        {
            if (fileLongLog == null) return;
            fileLongLog.WriteLine(text);
            fileLongLog.Flush();
        }
    }
}
