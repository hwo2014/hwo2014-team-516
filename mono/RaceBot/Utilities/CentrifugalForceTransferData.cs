﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaceBot.Utilities
{
    public class CentrifugalForceTransferData
    {
        private struct ForceTransferData
        {
            public double Radius;
            public double F;
            public double Alpha;


            public static double InterpolateAlpha(ForceTransferData first, ForceTransferData second,
                double F)
            {
                return (second.Alpha - first.Alpha) * (F - first.F) / (second.F - first.F) + first.Alpha;
            }

            internal static double InterpolateForce(ForceTransferData first, ForceTransferData second, double a)
            {
                return (second.F - first.F) * (a - first.Alpha) / (second.Alpha - first.Alpha) + first.F;
            }
        }


        private readonly List<List<ForceTransferData>> fcData;

        public CentrifugalForceTransferData()
        {
            fcData = CreateData();

            CreateData();
        }


        public double PredictF(double r, double a)
        {
            int lowRIndex = -1;
            int highRIndex = -1;
            for (int i = 0; i < fcData.Count; i++)
            {
                if (fcData[i][0].Radius <= r)
                    lowRIndex = i;

                if (fcData[i][0].Radius >= r)
                {
                    highRIndex = i;
                    break;
                }
            }

            if (lowRIndex == highRIndex)
            {
                // easy version, interpolate using only one data list
                return InterpolateForceList(a, lowRIndex);
            }
            else
            {
                if (lowRIndex < 0)
                    lowRIndex = fcData.Count - 2;
                if (highRIndex < 0)
                    highRIndex = 1;

                var low = InterpolateForceList(a, lowRIndex);
                var high = InterpolateForceList(a, highRIndex);

                return (high - low) * (r - fcData[lowRIndex][0].Radius) /
                       (fcData[highRIndex][0].Radius - fcData[lowRIndex][0].Radius) + low;
            }
        }


        public double PredictAlpha(double r, double F)
        {
            int lowRIndex = -1;
            int highRIndex = -1;
            for (int i = 0; i < fcData.Count; i++)
            {
                if (fcData[i][0].Radius <= r)
                    lowRIndex = i;

                if (fcData[i][0].Radius >= r)
                {
                    highRIndex = i;
                    break;
                }
            }

            if (lowRIndex == highRIndex)
            {
                // easy version, interpolate using only one data list
                return InterpolateAlphaList(F, lowRIndex);
            }
            else
            {
                if (lowRIndex < 0)
                    lowRIndex = fcData.Count - 2;
                if (highRIndex < 0)
                    highRIndex = 1;

                var low = InterpolateAlphaList(F, lowRIndex); 
                var high = InterpolateAlphaList(F, highRIndex);

                return (high - low) * (r - fcData[lowRIndex][0].Radius) /
                       (fcData[highRIndex][0].Radius - fcData[lowRIndex][0].Radius) + low;
            }
        }


        private double InterpolateAlphaList(double F, int lowRIndex)
        {
            if (F < fcData[lowRIndex][0].F)
            {
                return ForceTransferData.InterpolateAlpha(fcData[lowRIndex][0], fcData[lowRIndex][1], F);
            }

            for (int i = 1; i < fcData[lowRIndex].Count; i++)
            {    
                var node = fcData[lowRIndex][i];

                if (Math.Abs(F - node.F) <= 0.001)
                    return node.Alpha;

                if (F > fcData[lowRIndex][i - 1].F && F < node.F)
                {
                    return ForceTransferData.InterpolateAlpha(fcData[lowRIndex][i - 1],
                        node, F);
                }   
            }
            return ForceTransferData.InterpolateAlpha(fcData[lowRIndex][fcData[lowRIndex].Count - 2],
                    fcData[lowRIndex][fcData[lowRIndex].Count - 1], F);
        }


        private double InterpolateForceList(double a, int lowRIndex)
        {
            if (a < fcData[lowRIndex][0].Alpha)
            {
                return ForceTransferData.InterpolateForce(fcData[lowRIndex][0], fcData[lowRIndex][1], a);
            }

            for (int i = 1; i < fcData[lowRIndex].Count; i++)
            {
                var node = fcData[lowRIndex][i];

                if (Math.Abs(a - node.Alpha) <= 0.00001)
                    return node.F;

                if (a > fcData[lowRIndex][i - 1].Alpha && a < node.Alpha)
                {
                    return ForceTransferData.InterpolateForce(fcData[lowRIndex][i - 1],
                        node, a);
                }
            }
            return ForceTransferData.InterpolateForce(fcData[lowRIndex][fcData[lowRIndex].Count - 2],
                    fcData[lowRIndex][fcData[lowRIndex].Count - 1], a);
        }

        private const double normalFk = 1152;

        private static List<List<ForceTransferData>> CreateData()
        {
            var fcData = new List<List<ForceTransferData>>();

            // Add data from smallest radius to highest, otherwise the algorithm will break!

            fcData.Add(new List<ForceTransferData>()
            {
                new ForceTransferData() {Alpha = 0, F = 0, Radius = 40},
                new ForceTransferData() {Alpha = 0.000000000000001199041, F = 1152.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.022588873739561100000, F = 1200.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.057493276220714000000, F = 1272.4200000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.071028062713247500000, F = 1300.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.095893233481405800000, F = 1350.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.132724065786123000000, F = 1422.6400000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.146798278526892000000, F = 1450.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.172797614545780000000, F = 1500.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.199137275669740000000, F = 1550.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.225800920932508000000, F = 1600.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.252773476664595000000, F = 1650.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.291134917372137000000, F = 1720.1900000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.307590578061301000000, F = 1750.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.335410196624971000000, F = 1800.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.363488681782544000000, F = 1850.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.391815606978314000000, F = 1900.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.420381227341684000000, F = 1950.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.449176418876730000000, F = 2000.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.507421805693375000000, F = 2100.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.536856399982044000000, F = 2150.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.566489281955673000000, F = 2200.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.626323389626988000000, F = 2300.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.705952714440780000000, F = 2431.2800000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.748098646478092000000, F = 2500.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.809955425965058000000, F = 2600.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.872408802171763000000, F = 2700.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.935425920681601000000, F = 2800.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 0.998976836219607000000, F = 2900.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.089719920185830000000, F = 3041.4400000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.127572784771330000000, F = 3100.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.192569587999890000000, F = 3200.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.258003256603720000000, F = 3300.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.323854076666130000000, F = 3400.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.390103773800230000000, F = 3500.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.456735370148660000000, F = 3600.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.523733059145040000000, F = 3700.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.591082095412870000000, F = 3800.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.658768697624010000000, F = 3900.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.726779962499650000000, F = 4000.0000000- normalFk, Radius = 40},
                new ForceTransferData() {Alpha = 1.795103788430480000000, F = 2948, Radius = 40},
                new ForceTransferData() {Alpha = 1.84189281032503, F = 3016.22731046256, Radius = 40},
                new ForceTransferData() {Alpha = 1.89598739150517, F = 3094.86045545638, Radius = 40},
                new ForceTransferData() {Alpha = 1.94966427843405, F = 3172.63382897869, Radius = 40},
                new ForceTransferData() {Alpha = 2.00264968014026, F = 3249.1674095111, Radius = 40},
                new ForceTransferData() {Alpha = 2.0115083192369, F = 3261.94060272648, Radius = 40},
                new ForceTransferData() {Alpha = 2.02020651596132, F = 3274.4762890379, Radius = 40},
                new ForceTransferData() {Alpha = 2.050743220935, F = 3318.43737073708, Radius = 40},
                new ForceTransferData() {Alpha = 2.10801622381908, F = 3400.69159210886, Radius = 40},
                new ForceTransferData() {Alpha = 2.1598565125121, F = 3474.92856328752, Radius = 40},
                new ForceTransferData() {Alpha = 2.59068638384057, F = 4084.78824372232, Radius = 40},
                new ForceTransferData() {Alpha = 2.63733081401675, F = 4150.1276208713, Radius = 40},
                new ForceTransferData() {Alpha = 2.6834085344312, F = 4214.55323485319, Radius = 40},
                new ForceTransferData() {Alpha = 2.72891637859702, F = 4278.06779716602, Radius = 40},
                new ForceTransferData() {Alpha = 3.04999499896923, F = 4723.17487676654, Radius = 40},
                new ForceTransferData() {Alpha = 3.09063895337199, F = 4779.16576957142, Radius = 40},
                new ForceTransferData() {Alpha = 3.13071005394577, F = 4834.29446666273, Radius = 40},
                new ForceTransferData() {Alpha = 3.17021025276692, F = 4888.5680101179, Radius = 40},
                new ForceTransferData() {Alpha = 3.42942300447717, F = 5243.09164155862, Radius = 40},
                new ForceTransferData() {Alpha = 3.65592417619711, F = 5550.71283784144, Radius = 40},
                new ForceTransferData() {Alpha = 3.74137301660831, F = 5666.27910239085, Radius = 40},
                new ForceTransferData() {Alpha = 3.82951438782125, F = 5785.22208905064, Radius = 40},
                new ForceTransferData() {Alpha = 4.44631384866082, F = 6610.70981071452, Radius = 40},
                new ForceTransferData() {Alpha = 5.5452558296454, F = 8056.88275591155, Radius = 40},
                new ForceTransferData() {Alpha = 5.62002776530634, F = 8154.32287236055, Radius = 40},
                new ForceTransferData() {Alpha = 5.73295655714013, F = 8301.28209564709, Radius = 40},
            });

            fcData.Add(new List<ForceTransferData>()
                       {
                           new ForceTransferData() {Alpha = 0, F = 0, Radius = 60},
                           new ForceTransferData() {Alpha = 0.028830216, F = 50, Radius = 60},
                           new ForceTransferData() {Alpha = 0.058241603, F = 100, Radius = 60},
                           new ForceTransferData() {Alpha = 0.088199673, F = 150, Radius = 60},
                           new ForceTransferData() {Alpha = 0.118673221, F = 200, Radius = 60},
                           new ForceTransferData() {Alpha = 0.1496339, F = 250, Radius = 60},
                           new ForceTransferData() {Alpha = 0.181055871, F = 300, Radius = 60},
                           new ForceTransferData() {Alpha = 0.212915503, F = 350, Radius = 60},
                           new ForceTransferData() {Alpha = 0.245191113, F = 400, Radius = 60},
                           new ForceTransferData() {Alpha = 0.277862754, F = 450, Radius = 60},
                           new ForceTransferData() {Alpha = 0.310912023, F = 500, Radius = 60},
                           new ForceTransferData() {Alpha = 0.344321899, F = 550, Radius = 60},
                           new ForceTransferData() {Alpha = 0.378076605, F = 600, Radius = 60},
                           new ForceTransferData() {Alpha = 0.412161478, F = 650, Radius = 60},
                           new ForceTransferData() {Alpha = 0.446562869, F = 700, Radius = 60},
                           new ForceTransferData() {Alpha = 0.481268043, F = 750, Radius = 60},
                           new ForceTransferData() {Alpha = 0.516265096, F = 800, Radius = 60},
                           new ForceTransferData() {Alpha = 0.551542884, F = 850, Radius = 60},
                           new ForceTransferData() {Alpha = 0.587090951, F = 900, Radius = 60},
                           new ForceTransferData() {Alpha = 0.621462254, F = 2100.00 - normalFk, Radius = 60},
                           new ForceTransferData() {Alpha = 0.693804843, F = 2200.00 - normalFk, Radius = 60},
                           new ForceTransferData() {Alpha = 0.767086359, F = 2300.00 - normalFk, Radius = 60},
                           new ForceTransferData() {Alpha = 0.841246191, F = 2400.00 - normalFk, Radius = 60},
                           new ForceTransferData() {Alpha = 0.916229981, F = 2500.00 - normalFk, Radius = 60},
                           new ForceTransferData() {Alpha = 0.944143805, F = 1384.96, Radius = 60},
                           new ForceTransferData() {Alpha = 1.10533796564324, F = 1595.86683754097, Radius = 60},
                           new ForceTransferData() {Alpha = 1.12803054320246, F = 1625.23653530302, Radius = 60},
                           new ForceTransferData() {Alpha = 1.16765534766218, F = 1676.34522904997, Radius = 60},
                           new ForceTransferData() {Alpha = 1.20863694750823, F = 1728.9773108415, Radius = 60},
                           new ForceTransferData() {Alpha = 1.25102563321906, F = 1783.1836765579, Radius = 60},
                           new ForceTransferData() {Alpha = 1.29487365350314, F = 1839.01707744164, Radius = 60},
                           new ForceTransferData() {Alpha = 1.34023529368747, F = 1896.53219163555, Radius = 60},
                           new ForceTransferData() {Alpha = 1.36972461989774, F = 1933.79319311013, Radius = 60},
                           new ForceTransferData() {Alpha = 1.40008199399456, F = 1972.04793100169, Radius = 60},
                           new ForceTransferData() {Alpha = 1.47866291769277, F = 2070.60474584321, Radius = 60},
                           new ForceTransferData() {Alpha = 1.52447163587829, F = 2127.76104249137, Radius = 60},
                           new ForceTransferData() {Alpha = 1.56179550000138, F = 2174.17578296838, Radius = 60},
                           new ForceTransferData() {Alpha = 1.59873393360623, F = 2219.97863416732, Radius = 60},
                           new ForceTransferData() {Alpha = 1.63528034785133, F = 2265.16930421468, Radius = 60},
                           new ForceTransferData() {Alpha = 1.65296928399669, F = 2286.99817781382, Radius = 60},
                           new ForceTransferData() {Alpha = 1.85015910422631, F = 2528.51206459274, Radius = 60},
                           new ForceTransferData() {Alpha = 1.88377984624616, F = 2569.37489495165, Radius = 60},
                           new ForceTransferData() {Alpha = 1.91697791935007, F = 2609.63933506602, Radius = 60},
                           new ForceTransferData() {Alpha = 1.94975188697776, F = 2649.30868563277, Radius = 60},
                           new ForceTransferData() {Alpha = 1.98210073294172, F = 2688.38652455236, Radius = 60},
                           new ForceTransferData() {Alpha = 2.01402383751075, F = 2726.87668779334, Radius = 60},
                           new ForceTransferData() {Alpha = 2.38945162186401, F = 3174.53578827517, Radius = 60},
                           new ForceTransferData() {Alpha = 2.41554039263073, F = 3205.32970093079, Radius = 60},
                           new ForceTransferData() {Alpha = 2.44122828029401, F = 3235.61368004782, Radius = 60},
                           new ForceTransferData() {Alpha = 2.70764972479926, F = 3547.66996400205, Radius = 60},
                           new ForceTransferData() {Alpha = 2.46651851518037, F = 3265.39372888634, Radius = 60},
                           new ForceTransferData() {Alpha = 2.80330679293525, F = 3658.86500675642, Radius = 60},
                           new ForceTransferData() {Alpha = 2.87849308472212, F = 3745.97203043627, Radius = 60},
                           new ForceTransferData() {Alpha = 2.9561330759173, F = 3835.66236974191, Radius = 60},
                           new ForceTransferData() {Alpha = 3.03631474674348, F = 3928.02196343671, Radius = 60},
                       });

            fcData.Add(new List<ForceTransferData>()
                       {
                           new ForceTransferData() {Alpha = 0, F = 0, Radius = 90},
                           new ForceTransferData() {Alpha = 0.033883311, F = 1200 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.072846536, F = 1250 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.106542094, F = 1300 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.143839850, F = 1350 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.181735545, F = 1400.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.220197418, F = 1450.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.259196422, F = 1500.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.298705914, F = 1550.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.338701381, F = 1600.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.379160215, F = 1650.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.420061505, F = 1700.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.461385867, F = 1750.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.503115295, F = 1800.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.545233023, F = 1850.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.587723410, F = 1900.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.630571841, F = 1950.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.673764628, F = 2000.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.717288937, F = 2050.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.761132709, F = 2100.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.805284600, F = 2150.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.849733923, F = 2200.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.894470593, F = 2250.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.939485084, F = 2300.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 0.984768384, F = 2350.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 1.030311959, F = 2400.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 1.122147970, F = 2500.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 1.214933139, F = 2600.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 1.308613203, F = 2700.00 - normalFk, Radius = 90},
                           new ForceTransferData() {Alpha = 1.403138881, F = 1648 , Radius = 90},
                           new ForceTransferData() {Alpha = 1.57489674420119, F = 1827.60709949632, Radius = 90},
                           new ForceTransferData() {Alpha = 1.86929673068086, F = 2130.00604289703, Radius = 90},
                           new ForceTransferData() {Alpha = 2.1776218911035, F = 2440.53416044825, Radius = 90},
                           new ForceTransferData() {Alpha = 2.49880912031272, F = 2758.46745340744, Radius = 90},
                       });

            fcData.Add(new List<ForceTransferData>()
                       {
                           new ForceTransferData() {Alpha = 0, F = 0, Radius = 110},
                           new ForceTransferData() {Alpha = 0.039036297, F = 50, Radius = 110},
                           new ForceTransferData() {Alpha = 0.078859503, F = 100, Radius = 110},
                           new ForceTransferData() {Alpha = 0.119422922, F = 150, Radius = 110},
                           new ForceTransferData() {Alpha = 0.160684301, F = 200, Radius = 110},
                           new ForceTransferData() {Alpha = 0.202605258, F = 250, Radius = 110},
                           new ForceTransferData() {Alpha = 0.245150809, F = 300, Radius = 110},
                           new ForceTransferData() {Alpha = 0.288288953, F = 350, Radius = 110},
                           new ForceTransferData() {Alpha = 0.331990336, F = 400, Radius = 110},
                           new ForceTransferData() {Alpha = 0.376227947, F = 450, Radius = 110},
                           new ForceTransferData() {Alpha = 0.420976869, F = 500, Radius = 110},
                           new ForceTransferData() {Alpha = 0.466214055, F = 550, Radius = 110},
                           new ForceTransferData() {Alpha = 0.511918143, F = 600, Radius = 110},
                           new ForceTransferData() {Alpha = 0.558069279, F = 650, Radius = 110},
                           new ForceTransferData() {Alpha = 0.602777787, F = 700, Radius = 110},
                           new ForceTransferData() {Alpha = 0.651640011, F = 750, Radius = 110},
                           new ForceTransferData() {Alpha = 0.699026245, F = 800, Radius = 110},
                           new ForceTransferData() {Alpha = 0.746792595, F = 850, Radius = 110},
                           new ForceTransferData() {Alpha = 0.794924906, F = 900, Radius = 110},
                           new ForceTransferData() {Alpha = 0.84146387, F = 948, Radius = 110},
                           new ForceTransferData() { Alpha = 0.843046462, F = 970, Radius = 110}, // 970 + normalFk = 2122
                           new ForceTransferData() { Alpha = 0.890275623, F = 2150.00 - normalFk, Radius = 110 },
                           new ForceTransferData() { Alpha = 0.939416198, F = 2200.00 - normalFk, Radius = 110 },
                           new ForceTransferData() { Alpha = 1.038639840, F = 2300.00 - normalFk, Radius = 110 },
                           new ForceTransferData() { Alpha = 1.139052728, F = 2400.00 - normalFk, Radius = 110 },
                           new ForceTransferData() { Alpha = 1.240581258, F = 1348, Radius = 110 },
                           new ForceTransferData() {Alpha = 1.24674307215919, F = 1354.03535484605, Radius = 110},
                       });


            fcData.Add(new List<ForceTransferData>()
                       {
                           new ForceTransferData() {Alpha = 0, F = 0, Radius = 180},
                           new ForceTransferData() {Alpha = 0.167216650064921, F = 163.771105115843, Radius = 180},
                           new ForceTransferData() {Alpha = 0.17158395222845, F = 167.920628415844, Radius = 180},
                           new ForceTransferData() {Alpha = 0.175876435241426, F = 171.993499411912, Radius = 180},
                           new ForceTransferData() {Alpha = 0.180095099421904, F = 175.991000158909, Radius = 180},
                           new ForceTransferData() {Alpha = 0.184240944725753, F = 179.914397009853, Radius = 180},
                           new ForceTransferData() {Alpha = 0.188314969975993, F = 183.764940536355, Radius = 180},
                           new ForceTransferData() {Alpha = 0.201017847640359, F = 195.740324976976, Radius = 180},
                           new ForceTransferData() {Alpha = 0.213569143223286, F = 207.528050672033, Radius = 180},
                           new ForceTransferData() {Alpha = 0.2259678303377, F = 219.129817578281, Radius = 180},
                           new ForceTransferData() {Alpha = 0.238213063822235, F = 230.547372960785, Radius = 180},
                           new ForceTransferData() {Alpha = 0.250304169753029, F = 241.78250722697, Radius = 180},
                           new ForceTransferData() {Alpha = 0.262240635907362, F = 252.837049971506, Radius = 180},
                           new ForceTransferData() {Alpha = 0.274022102660018, F = 263.712866222744, Radius = 180},
                           new ForceTransferData() {Alpha = 0.285648354294129, F = 274.411852881829, Radius = 180},
                           new ForceTransferData() {Alpha = 0.297119310709106, F = 284.935935345984, Radius = 180},
                           new ForceTransferData() {Alpha = 0.308435019508698, F = 295.287064307793, Radius = 180},
                           new ForceTransferData() {Alpha = 0.322296391319957, F = 307.926406606107, Radius = 180},
                           new ForceTransferData() {Alpha = 0.339958383570574, F = 323.968309636605, Radius = 180},
                           new ForceTransferData() {Alpha = 0.358159406608045, F = 340.427954564805, Radius = 180},
                           new ForceTransferData() {Alpha = 0.425324528866108, F = 400.569779937126, Radius = 180},
                           new ForceTransferData() {Alpha = 0.434779141018661, F = 408.963928902452, Radius = 180},
                           new ForceTransferData() {Alpha = 0.44408805614366, F = 417.212151323734, Radius = 180},
                           new ForceTransferData() {Alpha = 0.453252469731114, F = 425.316496257017, Radius = 180},
                           new ForceTransferData() {Alpha = 0.462273621411878, F = 433.279006208425, Radius = 180},
                           new ForceTransferData() {Alpha = 0.491580421352072, F = 459.045282139854, Radius = 180},
                           new ForceTransferData() {Alpha = 0.521256260049016, F = 484.983113298933, Radius = 180},
                           new ForceTransferData() {Alpha = 0.518790740976492, F = 482.83386092148, Radius = 180},
                           new ForceTransferData() {Alpha = 0.516377239527884, F = 480.728963359989, Radius = 180},
                           new ForceTransferData() {Alpha = 0.514014608143919, F = 478.667479275313, Radius = 180},
                           new ForceTransferData() {Alpha = 0.522061661159507, F = 485.684979904115, Radius = 180},
                           new ForceTransferData() {Alpha = 0.529976665129218, F = 492.576748851853, Radius = 180},
                           new ForceTransferData() {Alpha = 0.527326750235381, F = 490.270570332612, Radius = 180},
                           new ForceTransferData() {Alpha = 0.535154983961905, F = 497.080003770966, Radius = 180},
                           new ForceTransferData() {Alpha = 0.542853667055658, F = 503.766916684691, Radius = 180},
                           new ForceTransferData() {Alpha = 0.550424320773249, F = 510.333218225773, Radius = 180},
                           new ForceTransferData() {Alpha = 0.557868478308491, F = 516.780800796991, Radius = 180},
                           new ForceTransferData() {Alpha = 0.565187682876111, F = 523.111539538128, Radius = 180},
                           new ForceTransferData() {Alpha = 0.572383485900209, F = 529.327291856062, Radius = 180},
                           new ForceTransferData() {Alpha = 0.57945744530278, F = 535.429896996538, Radius = 180},
                           new ForceTransferData() {Alpha = 0.586411123887664, F = 541.421175655485, Radius = 180},
                           new ForceTransferData() {Alpha = 0.593246087815758, F = 547.30292962794, Radius = 180},
                           new ForceTransferData() {Alpha = 0.599963905167159, F = 553.076941492518, Radius = 180},
                           new ForceTransferData() {Alpha = 0.606566144586433, F = 558.7449743297, Radius = 180},
                           new ForceTransferData() {Alpha = 0.613054374006965, F = 564.308771472127, Radius = 180},
                           new ForceTransferData() {Alpha = 0.619430159451094, F = 569.770056285196, Radius = 180},
                           new ForceTransferData() {Alpha = 0.625695063902093, F = 575.130531976396, Radius = 180},
                           new ForceTransferData() {Alpha = 0.631850646245089, F = 580.39188143183, Radius = 180},
                           new ForceTransferData() {Alpha = 0.637898460273393, F = 585.555767078388, Radius = 180},
                           new ForceTransferData() {Alpha = 0.643840053757355, F = 590.62383077032, Radius = 180},
                           new ForceTransferData() {Alpha = 0.649676967572708, F = 595.597693698685, Radius = 180},
                           new ForceTransferData() {Alpha = 0.655410734885641, F = 600.478956322551, Radius = 180},
                           new ForceTransferData() {Alpha = 0.661042880391989, F = 605.269198320625, Radius = 180},
                           new ForceTransferData() {Alpha = 0.66657491960769, F = 609.969978562205, Radius = 180},
                           new ForceTransferData() {Alpha = 0.680330690797868, F = 621.640400611767, Radius = 180},
                           new ForceTransferData() {Alpha = 0.697781690743682, F = 636.408706505077, Radius = 180},
                           new ForceTransferData() {Alpha = 0.715713579474879, F = 651.541523398717, Radius = 180},
                           new ForceTransferData() {Alpha = 0.734141314674698, F = 667.048892784677, Radius = 180},
                           new ForceTransferData() {Alpha = 0.753080371371438, F = 682.941168391432, Radius = 180},
                           new ForceTransferData() {Alpha = 0.772546761241436, F = 699.22902698074, Radius = 180},
                           new ForceTransferData() {Alpha = 0.80442001363037, F = 725.797892504241, Radius = 180},
                           new ForceTransferData() {Alpha = 0.810465668874378, F = 730.82370335385, Radius = 180},
                           new ForceTransferData() {Alpha = 0.9035026687841, F = 807.639815239681, Radius = 180},
                           new ForceTransferData() {Alpha = 1.85248062728564, F = 1549.36895285006, Radius = 180},
                           new ForceTransferData() {Alpha = 1.91119944621226, F = 1593.39602230922, Radius = 180},
                           new ForceTransferData() {Alpha = 1.97184166358976, F = 1638.68845553166, Radius = 180},
                           new ForceTransferData() {Alpha = 2.0344764285183, F = 1685.28720261584, Radius = 180},
                           new ForceTransferData() {Alpha = 2.09917553400346, F = 1733.23467307852, Radius = 180},
                           new ForceTransferData() {Alpha = 2.16601352174113, F = 1782.57479135577, Radius = 180},
                       });
            fcData.Add(new List<ForceTransferData>()
                       {
                           new ForceTransferData() {Alpha = 0, F = 0, Radius = 200},
                           new ForceTransferData() {Alpha = 0.00406029577675485, F = 3.89459785470217, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0160786794867584, F = 15.3845089178669, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0279715517733883, F = 26.6997527601679, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0397368745575769, F = 37.8416395384024, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0513728306348175, F = 48.8115396604708, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0628778121251974, F = 59.6108791567463, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0742504094368782, F = 70.2411352796341, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0854894007215689, F = 80.7038323213048, Radius = 200},
                           new ForceTransferData() {Alpha = 0.0965937418014062, F = 91.0005376401514, Radius = 200},
                           new ForceTransferData() {Alpha = 0.107562556547501, F = 101.132857886796, Radius = 200},
                           new ForceTransferData() {Alpha = 0.118395127691207, F = 111.10243542093, Radius = 200},
                           new ForceTransferData() {Alpha = 0.129090888049948, F = 120.910944910582, Radius = 200},
                           new ForceTransferData() {Alpha = 0.139649412150161, F = 130.5600901058, Radius = 200},
                           new ForceTransferData() {Alpha = 0.150070408230649, F = 140.05160077901, Radius = 200},
                           new ForceTransferData() {Alpha = 0.360709351, F = 1458.79 - normalFk, Radius = 200},
                           new ForceTransferData() {Alpha = 0.385277668, F = 1499.05 - normalFk, Radius = 200},
                           new ForceTransferData() {Alpha = 0.412417929, F = 1522.18 - normalFk, Radius = 200},
                           new ForceTransferData() {Alpha = 0.442139994, F = 1547.35 - normalFk, Radius = 200},
                           new ForceTransferData() {Alpha = 0.474610888, F = 1574.67 - normalFk, Radius = 200},
                           new ForceTransferData() {Alpha = 0.51048977, F = 452.65, Radius = 200},
                           new ForceTransferData() {Alpha = 0.535485954911613, F = 473.420987710317, Radius = 200},
                           new ForceTransferData() {Alpha = 0.619503311189084, F = 542.541564136613, Radius = 200},
                           new ForceTransferData() {Alpha = 0.704749353156015, F = 611.676064785635, Radius = 200},
                           new ForceTransferData() {Alpha = 0.79108430939913, F = 680.76891627718, Radius = 200},
                           new ForceTransferData() {Alpha = 0.847478011267332, F = 725.43878516411, Radius = 200},
                           new ForceTransferData() {Alpha = 0.87819160704026, F = 749.623134394874, Radius = 200},
                           new ForceTransferData() {Alpha = 0.909864387960766, F = 774.460582767069, Radius = 200},
                           new ForceTransferData() {Alpha = 0.94252963407976, F = 799.97103715963, Radius = 200},
                           new ForceTransferData() {Alpha = 0.976221869722186, F = 826.175081957218, Radius = 200},
                           new ForceTransferData() {Alpha = 1.01097691218063, F = 853.094004060976, Radius = 200},
                           new ForceTransferData() {Alpha = 1.04683192236336, F = 880.749818870855, Radius = 200},
                           new ForceTransferData() {Alpha = 1.08382545747641, F = 909.165297278375, Radius = 200},
                           new ForceTransferData() {Alpha = 1.12199752582228, F = 938.363993710215, Radius = 200},
                           new ForceTransferData() {Alpha = 1.16138964380167, F = 968.370275264693, Radius = 200},
                           new ForceTransferData() {Alpha = 1.20204489520766, F = 999.209351984927, Radius = 200},
                           new ForceTransferData() {Alpha = 3.2032879997644, F = 2411.77344480692, Radius = 200},
                           new ForceTransferData() {Alpha = 3.30524786504486, F = 2480.0242863626, Radius = 200},
                           new ForceTransferData() {Alpha = 3.4106787802787, F = 2550.33531569654, Radius = 200},
                           new ForceTransferData() {Alpha = 3.51970890658569, F = 2622.77609267288, Radius = 200},
                           new ForceTransferData() {Alpha = 3.39207092973915, F = 2537.94488046955, Radius = 200},
                       });


            fcData.Add(new List<ForceTransferData>()
                       {
                           new ForceTransferData() {Alpha = 0, F = 0, Radius = 220},
                           new ForceTransferData() {Alpha = 0.00567843076471378, F = 5.19176810052932, Radius = 220},
                           new ForceTransferData() {Alpha = 0.0153014418788092, F = 13.9637182779995, Radius = 220},
                           new ForceTransferData() {Alpha = 0.0248022081033956, F = 22.5923643445565, Radius = 220},
                           new ForceTransferData() {Alpha = 0.0341803938090839, F = 31.0792998406675, Radius = 220},
                           new ForceTransferData() {Alpha = 0.0434357801879496, F = 39.4261368286539, Radius = 220},
                           new ForceTransferData() {Alpha = 0.0525682585567907, F = 47.6345035266793, Radius = 220},
                           new ForceTransferData() {Alpha = 0.248372613691575, F = 217.909401515547, Radius = 220},
                           new ForceTransferData() {Alpha = 0.254408152622857, F = 223.00658748782, Radius = 220},
                           new ForceTransferData() {Alpha = 0.260343046213811, F = 228.011012930147, Radius = 220},
                           new ForceTransferData() {Alpha = 0.266178512779888, F = 232.92416939882, Radius = 220},
                           new ForceTransferData() {Alpha = 0.271915777736049, F = 237.747533019721, Radius = 220},
                           new ForceTransferData() {Alpha = 0.277556072208727, F = 242.482564226649, Radius = 220},
                           new ForceTransferData() {Alpha = 0.288550694974735, F = 251.693391296742, Radius = 220},
                           new ForceTransferData() {Alpha = 0.299172296316941, F = 260.568012000056, Radius = 220},
                           new ForceTransferData() {Alpha = 0.309430806324534, F = 269.117524310836, Radius = 220},
                           new ForceTransferData() {Alpha = 0.319336137714316, F = 277.352758740009, Radius = 220},
                           new ForceTransferData() {Alpha = 0.328898157340028, F = 285.284275572787, Radius = 220},
                           new ForceTransferData() {Alpha = 0.338126661095054, F = 292.922363252334, Radius = 220},
                           new ForceTransferData() {Alpha = 0.347031351898351, F = 300.27703778366, Radius = 220},
                           new ForceTransferData() {Alpha = 0.359802183710289, F = 310.798890505518, Radius = 220},
                           new ForceTransferData() {Alpha = 0.37189779518323, F = 320.736667979137, Radius = 220},
                           new ForceTransferData() {Alpha = 0.383349240758146, F = 330.120757932507, Radius = 220},
                           new ForceTransferData() {Alpha = 0.397667895467109, F = 341.821611937477, Radius = 220},
                           new ForceTransferData() {Alpha = 0.404439596806036, F = 347.342778518184, Radius = 220},
                           new ForceTransferData() {Alpha = 0.527237591526113, F = 446.161359925277, Radius = 220},
                           new ForceTransferData() {Alpha = 0.528192871949504, F = 446.920983551204, Radius = 220},
                           new ForceTransferData() {Alpha = 0.539032132806559, F = 455.530911030067, Radius = 220},
                           new ForceTransferData() {Alpha = 0.545053645560589, F = 460.306654404065, Radius = 220},
                           new ForceTransferData() {Alpha = 0.550085900058834, F = 464.293834680596, Radius = 220},
                           new ForceTransferData() {Alpha = 0.567094039994969, F = 477.743375123539, Radius = 220},
                           new ForceTransferData() {Alpha = 0.57546257774305, F = 484.34618697767, Radius = 220},
                           new ForceTransferData() {Alpha = 0.747221541772109, F = 617.873480361459, Radius = 220},
                           new ForceTransferData() {Alpha = 0.926645543588249, F = 753.810232415899, Radius = 220},
                           new ForceTransferData() {Alpha = 1.11314213044553, F = 891.907414251442, Radius = 220},
                           new ForceTransferData() {Alpha = 1.58658736611948, F = 1230.98490418634, Radius = 220},
                           new ForceTransferData() {Alpha = 1.63937413031851, F = 1267.94186155777, Radius = 220},
                           new ForceTransferData() {Alpha = 1.69387869296442, F = 1305.94618311712, Radius = 220},
                           new ForceTransferData() {Alpha = 1.75016252151274, F = 1345.03133065589, Radius = 220},
                           new ForceTransferData() {Alpha = 1.80828942682985, F = 1385.23194724092, Radius = 220},
                           new ForceTransferData() {Alpha = 1.86832565591905, F = 1426.58390187166, Radius = 220},
                           new ForceTransferData() {Alpha = 1.93033998839055, F = 1469.12433589623, Radius = 220},
                           new ForceTransferData() {Alpha = 1.9944038368279, F = 1512.89171125699, Radius = 220},
                           new ForceTransferData() {Alpha = 5.51530418751905, F = 3748.03189978844, Radius = 220},
                           new ForceTransferData() {Alpha = 5.65584477216446, F = 3832.81934575302, Radius = 220},
                           new ForceTransferData() {Alpha = 5.80089162708282, F = 3920.08684169724, Radius = 220},
                       });

            
            return fcData;
        }
    }
}
