using System;
using System.Collections.Generic;
using System.Linq;
using RaceBot.AI;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.Race
{
    public enum MyCarCollision
    {
        None,
        /// <summary> this car hit my car from the front</summary>
        FrontCollision,
        /// <summary> this car hit my car from the back</summary>
        BackCollision
    }

    /// <summary>
    /// A snapshot of a race car's situation.
    /// </summary>
    public class RaceCarSnapshot
    {
        public long GameTick { get; set; }

        public double Velocity { get; set; }

        public double DistanceFromMyCar { get; set; }

        public MyCarCollision Collision { get; set; }
    }
    
}
