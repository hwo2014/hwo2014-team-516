﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Send;

namespace RaceBot.Race
{
    public struct LaneSwitch
    {
        public int PieceIndex;
        public SwitchDirection Direction;


        public LaneSwitch(int pieceIndex, SwitchDirection direction) : this()
        {
            PieceIndex = pieceIndex;
            Direction = direction;
        }
    }
}
