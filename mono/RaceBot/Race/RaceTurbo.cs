﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Receive;

namespace RaceBot.Race
{
    public class RaceTurbo
    {
        public double Duration;
        public int DurationTicks;
        public double ActivationTime = -1;
        public double Factor;

        public RaceTurbo(TurboAvailable turbo)
        {
            Duration = turbo.turboDurationMilliseconds / 1000;
            DurationTicks = turbo.turboDurationTicks;
            Factor = turbo.turboFactor;
        }
    }
}
