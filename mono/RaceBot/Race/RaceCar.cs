﻿using System;
using RaceBot.AI;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;
using System.Collections.Generic;
using System.Linq;

namespace RaceBot.Race
{
    public class RaceCar
    {
        private readonly RaceKnowledge knowledge;

        /// <summary>
        /// "The angle depicts the car's slip angle. Normally this is zero, but when you go to a bend fast enough, 
        /// the car's tail will start to drift. Naturally, there are limits to how much you can drift without 
        /// crashing out of the track."
        /// </summary>
        /// <value>The angle.</value>
        public double Angle { get; private set; }

        public double LastAngle { get; private set; }

        [Obsolete("Use tick angle change instead")]
        public double AngleSpeed { get; private set; }
        public double TickAngleChange { get; private set; }
        public double TickAngleChangeSpeed { get; private set; }
        /// <summary>
        /// Harmonizer alpha value from previous tick, for log purposes only
        /// </summary>
        public double AngleHarmonizer { get; private set; }
        public double LastFc { get; private set; }
        public double LastRadius { get; private set; }
        private double velocity = 0;
        
        public double CurrentVelocity { get { return velocity; } }

        public double Acceleration
        {
            get
            {
                if (this.IsTurboActive)
                    return RaceMath.Acceleration * this.LastTurbo.Factor;
                return RaceMath.Acceleration;
            }
        }

        public PiecePosition PiecePosition { get; private set; }

        #region Turbo variables

        /// <summary>
        /// Returns true if the car has a turbo available, and false otherwise.
        /// </summary>
        public bool IsTurboAvailable { get { return AvailableTurbo != null; } }

        /// <summary>
        /// Returns true if this car has turbo active, and false otherwise.
        /// </summary>
        public bool IsTurboActive { get { return LastTurbo != null; } }

        /// <summary>
        /// The currently available turbo.
        /// </summary>
        public RaceTurbo AvailableTurbo { get; private set; }

        /// <summary>
        /// Latest turbo used by the car.
        /// </summary>
        public RaceTurbo LastTurbo { get; private set; }

        /// <summary>
        /// Total number of turbos this car received during the race.
        /// Turbos made available when crashed do not count, for example.
        /// </summary>
        public int TurbosReceived { get; private set; }

        /// <summary>
        /// Total number of turbos this car has used in the race.
        /// </summary>
        public int TurbosUsed { get; private set; }

        #endregion

        #region Lap times and race status

        /// <summary>
        /// Completed lap times in milliseconds.
        /// </summary>
        private List<int> lapTimes = new List<int>();

        /// <summary>
        /// Returns lap time for the given lap, in milliseconds.
        /// </summary>
        /// <param name="lap">Use real lap number e.g. first lap is 1, second lap is 2..</param>
        public int GetLapTimeForLap(int lap)
        {
            int lapIndex = lap - 1;

            if (lapIndex < lapTimes.Count) {
                return this.lapTimes [lapIndex];
            } else {
                return 0;
            }
        }

        /// <summary>
        /// Returns the best lap time in this session, in milliseconds.
        /// </summary>
        public int BestLapTime
        {
            get
            {
                if (this.lapTimes.Count > 0)
                {
                    int bestLapTime = int.MaxValue;
                    foreach (int lapTime in this.lapTimes)
                    {
                        if (lapTime < bestLapTime)
                        {
                            bestLapTime = lapTime;
                        }
                    }

                    return bestLapTime;
                }

                return 0;
            }
        }

        /// <summary>
        /// Returns true if the car is on its last lap, and false otherwise.
        /// </summary>
        public bool IsOnLastLap
        {
            get
            {
                return this.PiecePosition.lap+1 == this.knowledge.Session.laps;
            }
        }

        /// <summary>
        /// Gets the number of completed laps so far.
        /// </summary>
        /// <value>The completed laps.</value>
        public int CompletedLaps { get; private set; }

        /// <summary>
        /// Total race time in milliseconds when last crossing the finish line.
        /// </summary>
        public int TotalRaceTime { get; private set; }

        /// <summary>
        /// Ranking when last crossing the finish line.
        /// </summary>
        public int OverallRanking { get; private set; }

        #endregion

        /// <summary>
        /// Returns the current lane of the car.
        /// </summary>
        public Lane CurrentLane {
            get
            {
                return PiecePosition.lane;
            }
        }

        /// <summary>
        /// Returns the car's current track piece.
        /// </summary>
        public RaceTrackPiece CurrentTrackPiece
        {
            get
            {
                return this.knowledge.Track.CurrentTrackPiece(this.PiecePosition.pieceIndex);
            }
        }

        public CarId carId { get; private set; }
        public Dimensions Dimensions { get; private set; }
        public double ExpectedSpawnTime { get; private set; }

        /// <summary>
        /// Indicates whether this car is currently out of the track. Use this for most purposes,
        /// instead if IsCrashed, IsFinished or IsDisconnected separately.
        /// </summary>
        public bool IsOut
        {
            get
            {
                return (IsCrashed || IsDisconnected || IsFinished);
            }
        }

        /// <summary>
        /// Indicates whether this car is currently crashed.
        /// </summary>
        public bool IsCrashed { get; private set; }

        /// <summary>
        /// Indicates whether this race car has finished the race.
        /// </summary>
        public bool IsFinished { get; private set; }

        /// <summary>
        /// Indicates whether this car has disconnected/DNFd (did not finish).
        /// </summary>
        public bool IsDisconnected { get; private set; }

        /// <summary>
        /// Returns the total number of times this car has crashed during the race.
        /// </summary>
        public int CrashCount { get; private set; }

        public AiData AiData { get; set; }
        public double VelocityDelta { get; private set; }


        public RaceCar(RaceKnowledge knowledge, CarInit car)
        {
            this.knowledge = knowledge;
            carId = car.id;
            Dimensions = car.dimensions;
            PiecePosition = new PiecePosition();
			AiData = new AiData();
        }


        public void Update(IZTime zTime, CarPosition[] carPositions)
        {
            foreach (var carPosition in carPositions)
            {
                if (!carId.Equals(carPosition.id))
                {
                    // Not our car
                    continue;
                }
                if (PiecePosition.lane == null)
                    PiecePosition = carPosition.piecePosition;
                else if ( knowledge.Logger.LogMode == LogMode.None &&
                    PiecePosition.pieceIndex != carPosition.piecePosition.pieceIndex && knowledge.MyCar.Equals(this))
                {
                    var v = RaceMath.VelocityOnNextTick(CurrentVelocity) + Acceleration * knowledge.Throttle;
                    var d = (PiecePosition.inPieceDistance + v * RaceMath.TickDuration) -
                            carPosition.piecePosition.inPieceDistance;
                    knowledge.Logger.WriteLine("Last piece " + PiecePosition.pieceIndex + " lenght was: " + d +
                                               " lanes: " + PiecePosition.lane.startLaneIndex + " -> " +
                                               PiecePosition.lane.endLaneIndex);
                }

                AngleHarmonizer = RaceMath.HarmonicTenderizer(Angle, TickAngleChange, velocity);
                LastRadius = CurrentTrackPiece.GetRadius(CurrentLane);
                LastFc = RaceMath.CentrifugalForce(velocity, CurrentTrackPiece.GetRadius(CurrentLane));

                VelocityDelta = CalculateVelocity(zTime, carPosition) - velocity;
                if (zTime.Ticks == 0)
                    velocity = 0;
                else
                    velocity = CalculateVelocity(zTime, carPosition);

                
                TickAngleChangeSpeed = TickAngleChange - (carPosition.angle - Angle);
                TickAngleChange = carPosition.angle - Angle;
                AngleSpeed = (carPosition.angle - Angle) / zTime.D;
                LastAngle = Angle;
                Angle = carPosition.angle;
                PiecePosition = carPosition.piecePosition;
            }
            //Console.WriteLine("velocity: " + ZLib.FloatToText(CurrentVelocity));

            this.PrintLaneSwitches();

            AiData.AddSnapshot(this, knowledge.MyCar, knowledge.Track, zTime.Ticks);
        }

        public double CalculateVelocity(IZTime zTime, CarPosition carPosition)
        {
            double oldDistance = this.PiecePosition.inPieceDistance;
            double newDistance = carPosition.piecePosition.inPieceDistance;

            double velocity;

            if (PiecePosition.pieceIndex == carPosition.piecePosition.pieceIndex)
                velocity = (newDistance - oldDistance) / zTime.D;
            else
            {
                var oldPieceLength = knowledge.Track.Pieces[PiecePosition.pieceIndex].GetLength(PiecePosition.lane);

                velocity = ((newDistance + oldPieceLength) - oldDistance) / zTime.D;
            }
            if (double.IsNaN(velocity))
                velocity = 0;
            return velocity;
        }

        private int printedLaneSwitchForIndex = -1;

        void PrintLaneSwitches()
        {
            if (this.IsSwitchingLanes() &&
                printedLaneSwitchForIndex != PiecePosition.pieceIndex)
            {
                string switchMsg = String.Format("{0} switching lane from {1} to {2}.",
                    carId.name, PiecePosition.lane.startLaneIndex, PiecePosition.lane.endLaneIndex);
                knowledge.Logger.WriteLine(switchMsg);
                printedLaneSwitchForIndex = PiecePosition.pieceIndex;
            }

            if(!this.IsSwitchingLanes())
            {
                printedLaneSwitchForIndex = -1;
            }
        }

        /// <summary>
        /// Updates lap information for this car.
        /// </summary>
        /// <param name="lapFinished">Complete lapFinished message from server.</param>
        public void UpdateLaps(LapFinished lapFinished)
        {
            if (this.carId.Equals(lapFinished.car)) {
                
                this.lapTimes.Add (lapFinished.lapTime.millis);
                this.CompletedLaps = lapFinished.raceTime.laps;
                this.TotalRaceTime = lapFinished.raceTime.millis;
                this.OverallRanking = lapFinished.ranking.overall;
                //this.fastestLap = lapFinished.ranking.fastestLap; // WTF is the fastestLap value?
            }
        }

        /// <summary>
        /// Returns true if the car is switching lanes on this track piece, and false otherwise.
        /// </summary>
        public bool IsSwitchingLanes ()
        {
            return this.PiecePosition.lane.startLaneIndex != this.PiecePosition.lane.endLaneIndex;
        }

        /// <summary>
        /// Updates available turbos.
        /// </summary>
        /// <param name="turbo">"turboAvailable" message received.</param>
        public void TurboAvailable(TurboAvailable turbo)
        {
            // "If you receive a turboAvailable while being out of the track (between crash and spawn) then alas, you won't get a turbo. "
            if (!IsOut)
            {
                knowledge.Logger.WriteLine(this.carId.name + " turbo available!");
                TurbosReceived++;
                
                // "Turbos don't stack, i.e. if you already have a turbo available and receive turboAvailable message, you'll still have 1 turbo available"
                this.AvailableTurbo = new RaceTurbo(turbo);
            }
        }

        /// <summary>
        /// Notifies this car about turbo starting.
        /// </summary>
        public void TurboStarted()
        {
            if (LastTurbo == null)
            {
                LastTurbo = AvailableTurbo;
                LastTurbo.ActivationTime = knowledge.RaceTime.TotalD; 

                AvailableTurbo = null;

                TurbosUsed++;

                string message = string.Format("{0} turbo activated on tick {1}.", this.carId.name, knowledge.RaceTime.Ticks);
                knowledge.Logger.WriteLine(message);
            }
        }

        /// <summary>
        /// Notifies this car about turbo ending.
        /// </summary>
        public void TurboEnded()
        {
            LastTurbo = null;
            string message = String.Format("{0} turbo ended on tick {1}.", this.carId.name, knowledge.RaceTime.Ticks);
            knowledge.Logger.WriteLine(message);
        }

        /// <summary>
        /// Notify car about crashing.
        /// </summary>
        public void UpdateCrash()
        {
            this.IsCrashed = true;
            this.ExpectedSpawnTime = knowledge.RaceTime.TotalD + knowledge.SpawnTimeTicks / 60;
            this.ResetTurbo();
            this.CrashCount++;
        }

        /// <summary>
        /// Notify car about spawning.
        /// </summary>
        public void UpdateSpawn()
        {
            this.IsCrashed = false;
            this.AiData.Clear();
        }

        /// <summary>
        /// Notify car about receiving DNF (disconnect, program crash...)
        /// </summary>
        public void UpdateDnf()
        {
            this.IsDisconnected = true;
            this.ExpectedSpawnTime = Double.MaxValue;
            this.ResetTurbo();
        }

        /// <summary>
        /// Notify car about finishing the race.
        /// </summary>
        public void UpdateFinished()
        {
            this.IsFinished = true;
            this.ExpectedSpawnTime = Double.MaxValue;
            this.ResetTurbo();

            knowledge.Logger.WriteLine (String.Format ("{0} finished the race!", this.carId.name));
        }

        private void ResetTurbo()
        {
            this.AvailableTurbo = null;
            this.LastTurbo = null;
        }


        public double PredictAccel(int ticks, double currentTime)
        {
            if (IsTurboActive)
            {
                if (currentTime + ticks * RaceMath.TickDuration < LastTurbo.ActivationTime + LastTurbo.Duration + 0.00001)
                    return RaceMath.Acceleration * LastTurbo.Factor;
            }
            return RaceMath.Acceleration;
        }
    }
}
